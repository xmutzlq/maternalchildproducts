-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-ignorewarnings
-libraryjars libs/ctasdkforct.jar
-libraryjars libs/open_sdk.jar
-libraryjars /libs/android-support-v4.jar 
-dontwarn android.support.v4.** 

-keep class android.support.v4.** { *; } 
-keep public class * extends android.support.v4.** 
-keep public class * extends android.app.Fragment  
-keep public class * extends android.app.*Activity { public * ; }
-keep public class * extends android.app.Service { public * ; }
-keep public class * extends android.content.BroadcastReceiver { public * ; }
-keep public class * extends android.content.ContentProvider { public * ; }
-keep public class * extends android.view.View { public * ; }

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keep public class com.beat.android.maternalchild.application.MyAndroidApplication

-keep public class com.beat.android.maternalchild.utils.AndroidWebJS { public * ;}
-keep public class * implements com.beat.android.maternalchild.utils.IProguardFilter { public * ;}
