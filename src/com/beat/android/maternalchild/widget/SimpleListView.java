package com.beat.android.maternalchild.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

/**
 * 一个简单的ListView，由LinearLayout构造，模拟系统ListView
 * com.beat.android.maternalchild.widget.SimpleListView
 * @author Martin <br/>
 * create at 2016年1月22日 下午12:00:07
 */
public class SimpleListView extends LinearLayout {

    /**
     * 错误资源
     */
    private static final int INVALID = -1;

    private BaseAdapter adapter;

    /**
     * 数据观察器
     */
    private DataSetObserver dataSetObserver;

    private final LayoutInflater layoutInflater;

    /**
     * 分割线资源
     */
    private int dividerViewResourceId = INVALID;

    private View headerView;
    private View footerView;

    private OnItemClickListener itemClickListener;

    public SimpleListView(Context context) {
        this(context, null);
    }

    public SimpleListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        layoutInflater = LayoutInflater.from(getContext());
        setOrientation(VERTICAL);
    }

    public void setDividerView(int resourceId) {
        if (resourceId < 0) {
            throw new IllegalStateException("Resource Id cannot be negative");
        }
        dividerViewResourceId = resourceId;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public void setHeaderView(View view) {
        headerView = view;
    }

    public void setHeaderView(int resourceId) {
        headerView = layoutInflater.inflate(resourceId, this, false);
    }

    public void setFooterView(View view) {
        footerView = view;
    }

    public void setFooterView(int resourceId) {
        footerView = layoutInflater.inflate(resourceId, this, false);
    }

    public void setAdapter(BaseAdapter adapter) {
        if (adapter == null) {
            throw new NullPointerException("Adapter may not be null");
        }
        if (this.adapter != null && this.dataSetObserver != null) {
            this.adapter.unregisterDataSetObserver(dataSetObserver);
        }
        this.adapter = adapter;
        this.dataSetObserver = new AdapterDataSetObserver();
        this.adapter.registerDataSetObserver(dataSetObserver);
        resetList();
        refreshList();
    }

    /**
     * 构建ListView
     */
    private void refreshList() {
        if (headerView != null) {
            addView(headerView);
        }
        int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            final View view = adapter.getView(i, null, this);
            final int position = i;
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(adapter.getItem(position), view, position);
                    }
                }
            });
            addView(view);
            if (dividerViewResourceId != INVALID && i != count - 1) {
                addView(layoutInflater.inflate(dividerViewResourceId, this, false));
            }
        }
        if (footerView != null) {
            addView(footerView);
        }
    }

    /**
     * 清除数据
     */
    private void resetList() {
        this.removeAllViews();
        invalidate();
    }

    class AdapterDataSetObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            resetList();
            refreshList();
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Object item, View view, int position);
    }
}
