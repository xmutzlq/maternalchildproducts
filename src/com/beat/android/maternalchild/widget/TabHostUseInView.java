package com.beat.android.maternalchild.widget;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import com.beat.android.maternalchild.widget.AnimationController.ViewDealListener;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.TabHost.OnTabChangeListener;

public class TabHostUseInView extends android.widget.TabHost implements OnTabChangeListener{
	private OnClickTabListener mOnClickTabListener;
	private OnTabChangeListener mOnTabChangeListener;
	private ContentViewAdapter mContentViewAdapter;
	private View mCurrentContentView;
	private boolean isCacheViewEnabled = false;
	private boolean isReleaseRes = false;
	private FrameLayout mContentLayout;
	private FrameLayout mTempContentLayout;
	private HashMap<String,WeakReference<View>> mChildViews;
	
	public TabHostUseInView(Context context) {
		super(context);
		init();
	}

	public TabHostUseInView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init(){
		mChildViews = new HashMap<String, WeakReference<View>>();
		super.setOnTabChangedListener(this);
        mTempContentLayout = new FrameLayout(getContext());
        mTempContentLayout.setLayoutParams(new LayoutParams(0, 0));
        mTempContentLayout.setOnHierarchyChangeListener(new OnHierarchyChangeListener() {
			@Override
			public void onChildViewRemoved(View parent, View child) {
			}
			
			@Override
			public void onChildViewAdded(View parent, View child) {
				mTempContentLayout.removeView(child);
			}
		});
	}
	
	@Override
	public void setup() {
		mContentLayout = (FrameLayout) findViewById(android.R.id.tabcontent);
		if(!mTempContentLayout.equals(mContentLayout)){
			mContentLayout.removeAllViews();
			mContentLayout.setId(View.NO_ID);
			mTempContentLayout.setId(android.R.id.tabcontent);
			this.addView(mTempContentLayout);
		}
		super.setup();
	}
	
	@Override
	public void setCurrentTab(int index) {
		boolean isChange = index != getCurrentTab();
		super.setCurrentTab(index);
		if(mOnClickTabListener != null){
			mOnClickTabListener.onClickTab(index,isChange);
		}
		if(!isChange && mCurrentContentView == null){
			onTabChanged(getCurrentTabTag());
		}
	}
	
	public void setCacheViewEnabled(boolean isCacheViewEnabled){
		this.isCacheViewEnabled = isCacheViewEnabled;
	}
	
	public interface OnClickTabListener{
		public void onClickTab(int index,boolean isChange);
	}
	
	public void setOnClickTabListener(OnClickTabListener onClickTabListener){
		mOnClickTabListener = onClickTabListener;
	}
	
	@Override
	public void dispatchWindowFocusChanged(boolean hasFocus) {
		if (getCurrentView() != null){
			super.dispatchWindowFocusChanged(hasFocus);
        }
	}

	public TabContentFactory newTabContentFactory(){
		return new TabContentFactory() {

			@Override
			public View createTabContent(String tag) {
				FrameLayout contentView = new FrameLayout(getContext());
				contentView.setId(getCurrentTab());
				return contentView;
			}
		};
	}
	
	public interface ContentViewAdapter{
		public View getContentView(String tabId);
		public void onCreateContentView(View view);
		public void onResumeContentView(View view);
		public void onDestroyContentView(View view);
	}
	
	public void setContentViewAdapter(ContentViewAdapter contentViewAdapter){
		setContentViewAdapter(contentViewAdapter, false);
	}
	
	public void setContentViewAdapter(ContentViewAdapter contentViewAdapter,boolean isNeedReSetup){
		mContentViewAdapter = contentViewAdapter;
		if(isNeedReSetup){
			onTabChanged(getCurrentTabTag());
		}
	}
	
	@Override
	public void setOnTabChangedListener(OnTabChangeListener l) {
		mOnTabChangeListener = l;
	}

	@Override
	public void onTabChanged(String tabId) {
		toggleView(tabId);
		if(mOnTabChangeListener != null){
			mOnTabChangeListener.onTabChanged(tabId);
		}
	}
	
	private View newContentView(String tabId){
		isReleaseRes = false;
		if(mContentViewAdapter != null){
			View contentView = mContentViewAdapter.getContentView(tabId);
			contentView.setId(getCurrentTab());
			mChildViews.put(tabId, new WeakReference<View>(contentView));
			return contentView;
		}
		return null;
	}
	
	
	private View findChildViewByTab(String tabId){
		WeakReference<View> view = mChildViews.get(tabId);
		if(view != null){
			if(view.get() != null && view.get().getParent() != null){
				return view.get();
			}
		}
		mChildViews.remove(tabId);
		return null;
	}
	
	private void toggleView(String tabId) {
		if(mContentViewAdapter == null){
			return;
		}
		View tempNextContentView = findChildViewByTab(tabId);
		final View nextContentView;
		boolean isNewView = false;
		if(tempNextContentView == null){
			nextContentView = newContentView(tabId);
			isNewView = true;
		}else{
			nextContentView = tempNextContentView;
		}
		if (mCurrentContentView != null) {
			final View curContentView = mCurrentContentView;
			mCurrentContentView = nextContentView;
			cancelAnimation(curContentView);
			cancelAnimation(nextContentView);
			ViewDealListener dealListener = new ViewDealListener() {
				@Override
				public void dealView() {
					if( curContentView.getParent() != null && !curContentView.equals(mCurrentContentView) && !isCacheViewEnabled){
						if(mContentViewAdapter != null){
							mContentViewAdapter.onDestroyContentView(curContentView);
						}
						((ViewGroup)curContentView.getParent()).removeView(curContentView);
						nextContentView.setTag(null);
					}
				}
			};
			nextContentView.setTag(dealListener);
			dealListener.dealView();
		} else {
			mCurrentContentView = nextContentView;
		}
		if(isNewView){
			if(mContentViewAdapter != null){
				mContentViewAdapter.onCreateContentView(nextContentView);
			}
			mContentLayout.addView(nextContentView);
		}else{
			if(mContentViewAdapter != null){
				mContentViewAdapter.onResumeContentView(mCurrentContentView);
			}
		}
	}
	
	private void cancelAnimation(View view){
		if(view == null){
			return;
		}
		Animation animation = view.getAnimation();
		if(animation != null && !animation.hasStarted()){
			if(view.getTag() instanceof ViewDealListener){
				((ViewDealListener)view.getTag()).dealView();
				view.setTag(null);
			};
		}
		view.clearAnimation();
		view.setAnimation(null);
	}
	
	public View getCurContentView(){
		return mCurrentContentView;
	}
	
	public void releaseRes(){
		releaseRes(true);
	}

	private void releaseRes(boolean isNeedRemoveView){
		isReleaseRes = true;
		mCurrentContentView = null;
		if(mContentViewAdapter == null || getTabContentView() == null){
			return;
		}
		
		int count = mContentLayout.getChildCount();
		View childView = null;
		for(int i = 0; i < count;i++){
			childView = mContentLayout.getChildAt(i);
			mContentViewAdapter.onDestroyContentView(childView);
			if(isNeedRemoveView){
				mContentLayout.removeView(childView);
			}
		}
	}
	
	protected boolean isNeedWarningReleaseRes(){
		return true;
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		if(!isReleaseRes && isNeedWarningReleaseRes()){
			try{
				throw new RuntimeException("Not using the TabHost, must call releaseRes()");
			}catch (Exception e) {
				Log.w(this.getClass().getSimpleName(), e);
			}
		}
	}
}
