package com.beat.android.maternalchild.widget;

import com.beat.android.maternalchild.R;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class AlertDialog extends BaseDialog {
	private Button mBtnOk;
	private Button mBtnCancel;
	private TextView mTitleTV;
	private TextView mContentTV;
	private LinearLayout mContentLay;
	private View.OnClickListener mOkOnClickListener;
	private View.OnClickListener mCancelOnClickListener;
	private View.OnClickListener mBackOnClickListener;

	public AlertDialog(Context context) {
		super(context, R.style.CustomDialogNoPadding);
		setCanceledOnTouchOutside(true);
		setContentView(R.layout.dialog_alert_common); 
		 
		mTitleTV = (TextView) findViewById(R.id.dialog_title);
		mContentTV = (TextView) findViewById(R.id.dialog_content);
		
		mBtnOk = (Button) findViewById(R.id.dialog_ok);
		mBtnCancel = (Button) findViewById(R.id.dialog_cancel);
		
		findViewById(R.id.btn_lay).setVisibility(View.GONE);
		
		mContentLay = (LinearLayout) findViewById(R.id.dialog_content_lay);
		
		mBtnOk.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mOkOnClickListener != null){
					mOkOnClickListener.onClick(v);
				}
				dismiss();
			}
		});
		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(mCancelOnClickListener != null){
					mCancelOnClickListener.onClick(v);
				}
				dismiss();
			}
		});
		setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				return false;
			}
		});
	}
	
	public void setTitle(int resId){
		if(resId == -1){
			return;
		}
		mTitleTV.setText(resId);
	}
	
	public void setTitle(String title){
		if(TextUtils.isEmpty(title)){
			return;
		}
		mTitleTV.setText(title);
	}
	
	private void resetContentGravity(){
		String str = mContentTV.getText().toString().trim();
		if (!TextUtils.isEmpty(str)) {
			if (str.length() <= 10) {
				mContentTV.setGravity(Gravity.CENTER);
			} else {
				mContentTV.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
			}
		}
	}
	
	public void setContent(int resId){
		mContentTV.setText(resId);
		resetContentGravity();
		mContentLay.setVisibility(View.GONE);
	}
	
	public void setContent(String content) {
		mContentTV.setText(content);
		resetContentGravity();
		mContentLay.setVisibility(View.GONE);
	}
	
	public void setContentLayout(View view){
		if(view != null){
			mContentLay.addView(view);
			mContentTV.setVisibility(View.GONE);
		}
	}
	
	/** 设置按钮文字和事件
	 * @param okBtnTextId 确认按钮的文字
	 * @param okOnClickListener 确认按钮的事件
	 * @param cancelBtnTextId 取消按钮的文字
	 * @param cancelOnClickListener 取消按钮的事件
	 */
	public void setButton(int okBtnTextId, View.OnClickListener okOnClickListener,
			int cancelBtnTextId, View.OnClickListener cancelOnClickListener){
		
		mBackOnClickListener = cancelOnClickListener;
		if(Build.VERSION.SDK_INT >= 14){//4.0
			mBtnOk.setText(cancelBtnTextId);
			mBtnOk.setTextColor(getContext().getResources().getColor(R.color.btn_blue_content));
			mBtnOk.setBackgroundResource(R.drawable.bg_light_btn);
			
			mBtnCancel.setText(okBtnTextId);
			mBtnCancel.setTextColor(getContext().getResources().getColor(R.color.white));
			mBtnCancel.setBackgroundResource(R.drawable.bg_normal_btn);
			
			mOkOnClickListener = cancelOnClickListener;
			mCancelOnClickListener = okOnClickListener;
		}else{
			mBtnOk.setText(okBtnTextId);
			mBtnCancel.setText(cancelBtnTextId);
			mOkOnClickListener = okOnClickListener;
			mCancelOnClickListener = cancelOnClickListener;
		}
		findViewById(R.id.btn_lay).setVisibility(View.VISIBLE);
	}
	
	/**
	 * 设置按钮是否可见
	 * @param visible
	 */
	public void setButtonVisible(boolean visible) {
		findViewById(R.id.btn_lay).setVisibility(visible ? View.VISIBLE : View.GONE);
	}
}
