package com.beat.android.maternalchild.widget;

import com.beat.android.maternalchild.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;

/**
 * 可以下拉刷新的ScrollView
 * @author shizq
 *
 */
public class PullDownScrollView extends BorderScrollView {
	
	private float mLastY = -1; // save event y
	
	private XListViewHeader mHeaderView;
	private RelativeLayout mHeaderViewContent;
	private TextView mHeaderTimeView;
	protected int mHeaderViewHeight;
	
	private boolean mEnablePullRefresh = true;
	private boolean mPullRefreshing = false; // is refreashing.
	private boolean mCanPullDown = true;
	
	private int mScrollBack;
	private final static int SCROLLBACK_HEADER = 0;
	
	private final static int SCROLL_DURATION = 400; // scroll back duration

	private RefreshListener mRefreshListener;

	private Scroller mScroller;

	private View mTopHeaderView;

	private android.view.ViewGroup.LayoutParams mTopHeaderViewParams;

	private SVOnScrollChangeListener mListener;
	
	private final static float OFFSET_RADIO = 1.8f; // support iOS like pull feature.

	public PullDownScrollView(Context context) {
		super(context);
		init();
	}

	public PullDownScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	private void init(){
//		setClickable(true);
		mScroller = new Scroller(getContext(), new DecelerateInterpolator());
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		if(getChildCount()> 0){
			if(getChildCount()> 1){
				throw new IllegalStateException("PullDownScrollView can host only one direct child!");
			}
			View contentView = getChildAt(0);
			removeAllViews();
			if(contentView instanceof ViewGroup){
				((ViewGroup)contentView).addView(initHeaderView(), 0);
				if(mTopHeaderView != null){
					if(mTopHeaderViewParams != null){
						((ViewGroup)contentView).addView(mTopHeaderView, 0, mTopHeaderViewParams);
					}else{
						((ViewGroup)contentView).addView(mTopHeaderView, 0);
					}
				}
				super.addView(contentView);
			}else{
				LinearLayout layout = new LinearLayout(getContext());
				layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
				layout.setOrientation(LinearLayout.VERTICAL);
				if(mTopHeaderView != null){
					if(mTopHeaderViewParams != null){
						layout.addView(mTopHeaderView, 0, mTopHeaderViewParams);
					}else{
						layout.addView(mTopHeaderView, 0);
					}
				}
				layout.addView(initHeaderView());
				layout.addView(contentView);
				super.addView(layout);
			}
			
		}
	}
	
	/**
	 * set last refresh time
	 * 
	 * @param time
	 */
	public void setRefreshTime(String time) {
		mHeaderTimeView.setText(time);
	}
	
	public void addTopHeaderView(View child){
		addTopHeaderView(child, null);
	}
	
	public void addTopHeaderView(View child, android.view.ViewGroup.LayoutParams params){
		if(mTopHeaderView != null)
			return;
		
		try {
			mTopHeaderView = child;
			mTopHeaderViewParams = params;
			
			if(getChildCount()> 0){
				View contentView = getChildAt(0);
				if(params != null){
					((ViewGroup)contentView).addView(child, 0, params);
				}else{
					((ViewGroup)contentView).addView(child, 0);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private View initAddView(View child){
		if(child instanceof ViewGroup){
			((ViewGroup)child).addView(initHeaderView(), 0);
			return child;
		}else{
			LinearLayout layout = new LinearLayout(getContext());
			layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			layout.setOrientation(LinearLayout.VERTICAL);
			if(mTopHeaderView != null){
				if(mTopHeaderViewParams != null){
					layout.addView(mTopHeaderView, 0, mTopHeaderViewParams);
				}else{
					layout.addView(mTopHeaderView, 0);
				}
			}
			layout.addView(initHeaderView());
			layout.addView(child);
			return layout;
		}
	}
	
	@Override
	public void addView(View child) {
		if(getChildCount()> 0){
			throw new IllegalStateException("PullDownScrollView can host only one direct child!");
		}
		super.addView(initAddView(child));
	}
	
	@Override
	public void addView(View child, int index) {
		if(getChildCount()> 0){
			throw new IllegalStateException("PullDownScrollView can host only one direct child!");
		}
		super.addView(initAddView(child), index);
	}
	
	@Override
	public void addView(View child, android.view.ViewGroup.LayoutParams params) {
		if(getChildCount()> 0){
			throw new IllegalStateException("PullDownScrollView can host only one direct child!");
		}
		super.addView(initAddView(child), params);
	}
	
	@Override
	public void addView(View child, int index, android.view.ViewGroup.LayoutParams params) {
		if(getChildCount()> 0){
			throw new IllegalStateException("PullDownScrollView can host only one direct child!");
		}
		super.addView(initAddView(child), index, params);
	}
	
	private XListViewHeader initHeaderView(){
		mHeaderView = new XListViewHeader(getContext());
		mHeaderViewContent = (RelativeLayout) mHeaderView
				.findViewById(R.id.xlistview_header_content);
		mHeaderTimeView = (TextView) mHeaderView
				.findViewById(R.id.xlistview_header_time);
		
		// init header height
		mHeaderView.getViewTreeObserver().addOnGlobalLayoutListener(
				new OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						mHeaderViewHeight = mHeaderViewContent.getHeight();
							getViewTreeObserver().removeGlobalOnLayoutListener(this);
						}
					});
		return mHeaderView;
	}
	
	/**
	 * enable or disable pull down refresh feature.
	 * 
	 * @param enable
	 */
	public void setPullRefreshEnable(boolean enable) {
		mEnablePullRefresh = enable;
		if (!mEnablePullRefresh) { // disable, hide the content
			mHeaderViewContent.setVisibility(View.INVISIBLE);
		} else {
			mHeaderViewContent.setVisibility(View.VISIBLE);
		}
	}
	
	private void updateHeaderHeight(float delta) {
		mHeaderView.setVisiableHeight((int) delta
				+ mHeaderView.getVisiableHeight());
		if (mEnablePullRefresh && !mPullRefreshing) { // 未处于刷新状态，更新箭头
			if (mHeaderView.getVisiableHeight() > mHeaderViewHeight) {
				mHeaderView.setState(XListViewHeader.STATE_READY);
			} else {
				mHeaderView.setState(XListViewHeader.STATE_NORMAL);
			}
		}
	}
	
	
	@Override
	public void computeScroll() {
		if (mScroller.computeScrollOffset()) {
			if (mScrollBack == SCROLLBACK_HEADER) {
				mHeaderView.setVisiableHeight(mScroller.getCurrY());
			}
			postInvalidate();
		}
		super.computeScroll();
	}
	
	/**
	 * stop refresh, reset header view.
	 */
	public void stopRefresh() {
		if (mPullRefreshing == true) {
			mPullRefreshing = false;
			resetHeaderHeight();
		}
	}
	
	/**
	 * reset header view's height.
	 */
	private void resetHeaderHeight() {
		int height = mHeaderView.getVisiableHeight();
		if (height == 0) // not visible.
			return;
		// refreshing and header isn't shown fully. do nothing.
		if (mPullRefreshing && height <= mHeaderViewHeight) {
			return;
		}
		int finalHeight = 0; // default: scroll back to dismiss header.
		// is refreshing, just scroll back to show all the header.
		if (mPullRefreshing && height > mHeaderViewHeight) {
			finalHeight = mHeaderViewHeight;
		}
		mScrollBack = SCROLLBACK_HEADER;
		mScroller.startScroll(0, height, 0, finalHeight - height,
				SCROLL_DURATION);
		// trigger computeScroll
		invalidate();
	}

//	@Override
//	public boolean dispatchTouchEvent(MotionEvent ev) {
//		
//		if (mLastY == -1) {
//			mLastY = ev.getRawY();
//		}
//
//		switch (ev.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			mCanPullDown = isOnTop();
//			mLastY = ev.getRawY();
//			break;
//		case MotionEvent.ACTION_MOVE:
//			final float deltaY = ev.getRawY() - mLastY;
//			mLastY = ev.getRawY();
//			if (deltaY > 0 && mCanPullDown) {
//				// the first item is showing, header has shown or pull down.
//				updateHeaderHeight(deltaY / OFFSET_RADIO);
//			}
//			break;
//		default:
//			mLastY = -1; // reset
//			mCanPullDown = isOnTop();
//			if (0 == 0) {
//				// invoke refresh
//				if (mHeaderView.getVisiableHeight() > mHeaderViewHeight) {
//					mPullRefreshing = true;
//					mHeaderView.setState(XListViewHeader.STATE_REFRESHING);
//					if (mRefreshListener != null) {
//						mRefreshListener.onRefresh();
//					}
//				}
//				resetHeaderHeight();
//			}
//			break;
//		}
//		return super.dispatchTouchEvent(ev);
//	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		if (mLastY == -1) {
			mLastY = ev.getRawY();
		}

		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mCanPullDown = isOnTop();
			mLastY = ev.getRawY();
			break;
		case MotionEvent.ACTION_MOVE:
			final float deltaY = ev.getRawY() - mLastY;
//			mLastY = ev.getRawY();
			if (deltaY > 0 && mCanPullDown) {
//				// the first item is showing, header has shown or pull down.
//				updateHeaderHeight(deltaY / OFFSET_RADIO);
				return true;
			}
//		default:
//			mLastY = -1; // reset
//			mCanPullDown = isOnTop();
//			if (0 == 0) {
//				// invoke refresh
//				if (mHeaderView.getVisiableHeight() > mHeaderViewHeight) {
//					mPullRefreshing = true;
//					mHeaderView.setState(XListViewHeader.STATE_REFRESHING);
//					if (mRefreshListener != null) {
//						mRefreshListener.onRefresh();
//					}
//				}
//				resetHeaderHeight();
//			}
//			break;
		}
		return super.onInterceptTouchEvent(ev);
	}

	private float origiDeltaY;
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (mLastY == -1) {
			mLastY = event.getRawY();
		}

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mLastY = event.getRawY();
			break;
		case MotionEvent.ACTION_MOVE:
			final float deltaY = event.getRawY() - mLastY;
			mLastY = event.getRawY();
			
			if ((deltaY > 0 && mCanPullDown) || (mHeaderView.getVisiableHeight() > 0 && deltaY < 0 && mCanPullDown)) {
				// the first item is showing, header has shown or pull down.
				updateHeaderHeight(deltaY / OFFSET_RADIO);
				return true;
			}
			if(origiDeltaY > deltaY && mHeaderView.getVisiableHeight() > 0){
				System.out.println("getScrollY():"+getScrollY()+" and deltaY:"+deltaY);
				return true;
			}
			origiDeltaY = deltaY;
			break;
		default:
			mLastY = -1; // reset
			origiDeltaY = 0;
			if (0 == 0) {
				// invoke refresh
				if (mHeaderView.getVisiableHeight() > mHeaderViewHeight) {
					mPullRefreshing = true;
					mHeaderView.setState(XListViewHeader.STATE_REFRESHING);
					if (mRefreshListener != null) {
						mRefreshListener.onRefresh();
					}
				}
				resetHeaderHeight();
			}
			break;
		}
//		System.out.println("super.onTouchEvent(event)");
		return super.onTouchEvent(event);
	}
	
	@Override
	protected void onScrollChanged(int x, int y, int oldx, int oldy) {
		super.onScrollChanged(x, y, oldx, oldy);
		if(mListener != null){
			mListener.onScrollChanged(x, y, oldx, oldy);
		}
	}
	
	public void setOnScrollChangedListener(SVOnScrollChangeListener listener){
		mListener = listener;
	}
	
	public void setRefreshListener(RefreshListener listener){
		mRefreshListener = listener;
	}
	
	public interface SVOnScrollChangeListener{
		public void onScrollChanged(int x, int y, int oldx, int oldy);
	}
	
	/**
	 * implements this interface to get refresh/load more event.
	 */
	public interface RefreshListener {
		public void onRefresh();
	}


}
