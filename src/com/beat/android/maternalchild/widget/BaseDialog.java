package com.beat.android.maternalchild.widget;

import com.beat.android.maternalchild.base.AbsContextActivity;
import com.beat.android.maternalchild.base.AbsContextActivity.LifeCycleListener;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

public class BaseDialog extends Dialog implements LifeCycleListener{
public AbsContextActivity mAbsContextActivity;
	
	public BaseDialog(Context context) {
		super(context);
		init(context);
	}
	
	protected BaseDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		init(context);
	}
	
	public BaseDialog(Context context, int theme) {
		super(context, theme);
		init(context);
	}
	
	private void init(Context context){
		if(context instanceof AbsContextActivity){
			mAbsContextActivity = (AbsContextActivity) context;
		}
		setGravity(getDefaultGravity());
	}

	public void setGravity(int gravity){
		Window window = getWindow();    
		window.setGravity(gravity);
	}
	
	private int getDefaultGravity(){
		return Gravity.BOTTOM;
	}
	
	@Override
	public void show() {
		super.show();
		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.width = getWindow().getWindowManager().getDefaultDisplay().getWidth();
		getWindow().setAttributes(lp);
		if(mAbsContextActivity != null){
			mAbsContextActivity.registerLifeCycleListener(this);
		}
	}

	@Override
	public void dismiss() {
		super.dismiss();
		if(mAbsContextActivity != null){
			mAbsContextActivity.unregisterLifeCycleListener(this);
		}
	}

	@Override
	public void onActivityDestroy() {
		if(isShowing()){
			dismiss();
		}
	}
}
