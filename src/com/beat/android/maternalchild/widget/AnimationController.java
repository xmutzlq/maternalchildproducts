package com.beat.android.maternalchild.widget;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.animation.Animation;

public class AnimationController {
	
	private static final class DisplayNextView implements
			Animation.AnimationListener {
		private final View view;
		private final ViewDealListener viewDealListener;

		private DisplayNextView(View view,ViewDealListener viewDealListener) {
			this.view = view;
			this.viewDealListener = viewDealListener;
		}

		@Override
		public void onAnimationStart(Animation animation) {
			
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			Handler handler = view.getHandler();
			if(handler == null){
				handler = new Handler(Looper.getMainLooper());
			}
			handler.post(new Runnable() {
				@Override
				public void run() {
					if (viewDealListener != null) {
						viewDealListener.dealView();
					}
				}
			});
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}
	}

	private static final class DisplayNextView2 implements
			Animation.AnimationListener {

		private final boolean isWillShow;
		private final View view;
		private final ViewDealListener viewDealListener;

		private DisplayNextView2(View view, boolean isWillShow,
				ViewDealListener viewDealListener) {
			this.view = view;
			this.isWillShow = isWillShow;
			this.viewDealListener = viewDealListener;
		}

		@Override
		public void onAnimationStart(Animation animation) {
			view.setVisibility(View.VISIBLE);
		}

		@Override
		public void onAnimationEnd(Animation animation) {
			if (!isWillShow) {
				view.setVisibility(View.GONE);
			}
			if (viewDealListener != null) {
				viewDealListener.dealView();
			}
		}

		@Override
		public void onAnimationRepeat(Animation animation) {
		}
	}

	public static interface ViewDealListener {

		public void dealView();

	}
}
