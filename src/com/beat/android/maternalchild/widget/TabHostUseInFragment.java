package com.beat.android.maternalchild.widget;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TabHost;

/**
 * Fragment嵌套Fragment
 * @author Administrator
 *
 */
public class TabHostUseInFragment extends android.widget.TabHost
{
	private HashMap<String, TabInfo> mHomeTabs = new HashMap<String, TabInfo>();
	private TabInfo mHomeLastTab;
	
	private FragmentActivity mActivity;
	private FragmentManager mFragmentManager;
	
	private List<ITabChangeObserver> tabChangeListeners = new ArrayList<ITabChangeObserver>();
	
	public TabHostUseInFragment(Context context)
	{
		super(context);
		init();
	}

	public TabHostUseInFragment(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	private void init(){
		setOnTabChangedListener(mTabChangeListener);
	}

	/**
	 * 注册tabchange监听
	 * @param listener
	 */
	public void registTabChangeObserver(ITabChangeObserver listener){
		if(tabChangeListeners != null){
			if(!tabChangeListeners.contains(listener)){
				tabChangeListeners.add(listener);
			}
		}
	}
	
	/**
	 * 注销tabchange监听
	 * @param listener
	 */
	public void unregistTabChangeObserver(ITabChangeObserver listener){
		if(tabChangeListeners != null && tabChangeListeners.size() > 0){
			tabChangeListeners.remove(listener);
		}
	}
	
	public void setFragmentParam(FragmentActivity activity, FragmentManager manager){
		mActivity = activity;
		mFragmentManager = manager;
	}
	
	/**
	 * 添加TAB项
	 * 
	 * @author liuyun
	 * @date 2015-7-6 下午5:22:31
	 * @param tabSpec
	 * @param clss
	 * @param args
	 */
	public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args, boolean isInitTab)
	{
		tabSpec.setContent(new DummyTabFactory(getContext()));
		String tag = tabSpec.getTag();
		TabInfo info = new TabInfo(tag, clss, args);
		mHomeTabs.put(tag, info);
		addTab(tabSpec);
		if(isInitTab)
		{
			FragmentTransaction ft = mFragmentManager.beginTransaction();
			info.fragment = Fragment.instantiate(getContext(), info.clss.getName(), info.args);
			ft.add(android.R.id.tabcontent, info.fragment, info.tag);
			ft.hide(info.fragment);
			ft.commit();
		}
	}
	
	private OnTabChangeListener mTabChangeListener = new OnTabChangeListener()
	{
		
		@Override
		public void onTabChanged(String tabId)
		{
			actionTabChange(tabId);
			notifyTabChange(tabId);
		}
	};
	
	private void actionTabChange(String tabId)
	{
		TabInfo newTab = mHomeTabs.get(tabId);
		if (mHomeLastTab != newTab)
		{
			FragmentTransaction ft = mFragmentManager.beginTransaction();
			if (mHomeLastTab != null)
			{
				if (mHomeLastTab.fragment != null && mHomeLastTab.fragment.isVisible())
				{
					ft.hide(mHomeLastTab.fragment);
				}
			}
			if (newTab != null)
			{
				if (newTab.fragment == null)
				{
					newTab.fragment = Fragment.instantiate(mActivity, newTab.clss.getName(), newTab.args);
					ft.add(android.R.id.tabcontent, newTab.fragment, newTab.tag);
				} else
				{
					ft.show(newTab.fragment);
				}
			}

			mHomeLastTab = newTab;
			ft.commitAllowingStateLoss();
			mFragmentManager.executePendingTransactions();
		}
	}
	
	private void notifyTabChange(String tabId){
		if(tabChangeListeners != null && tabChangeListeners.size() > 0){
			for (ITabChangeObserver observer : tabChangeListeners) {
				observer.onTabChange(tabId);
			}
		}
	}
	
	private static class TabInfo
	{
		private final String tag;
		private final Class<?> clss;
		private final Bundle args;
		private Fragment fragment;

		public TabInfo(String _tag, Class<?> _class, Bundle _args)
		{
			tag = _tag;
			clss = _class;
			args = _args;
		}
	}
	
	private static class DummyTabFactory implements TabHost.TabContentFactory
	{
		private final Context mContext;

		public DummyTabFactory(Context context)
		{
			mContext = context;
		}

		@Override
		public View createTabContent(String tag)
		{
			View v = new View(mContext);
			v.setMinimumWidth(0);
			v.setMinimumHeight(0);
			return v;
		}
	}
	
	public static interface ITabChangeObserver{
		/**
		 * tab变化时的通知
		 * @param tabId
		 */
		public void onTabChange(String tabId);
	}
}
