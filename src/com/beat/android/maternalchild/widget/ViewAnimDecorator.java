package com.beat.android.maternalchild.widget;

import com.beat.android.maternalchild.base.BaseApplication;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
/**
 * 控制View的显示和隐藏动画
 * @author Administrator
 *
 */
public class ViewAnimDecorator{
	public static void showView(View view){
		showView(view, true);
	}
	
	public static void showView(View view,boolean isStartAnim){
		if(view == null){
			return;
		}
		if(isStartAnim){
			clearHideRunnable(view);
			view.setAnimation(AnimationUtils.loadAnimation(BaseApplication.getInstance(), android.R.anim.fade_in));
		}else{
			view.setAnimation(null);
		}
		view.setVisibility(View.VISIBLE);
	}
	
	private static void clearHideRunnable(final View view){
//		Object runnable = view.getTag();
//		view.setAnimation(null);
//		if(runnable instanceof Runnable){
//			BaseApplication.getHandler().removeCallbacks((Runnable) runnable);
//		}
	}
	
	public static void hideView(final View view){
		hideView(view, true);
	}
	
	public static void hideView(final View view,boolean isStartAnim){
		if(view == null){
			return;
		}
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				view.setAnimation(null);
				view.setVisibility(View.INVISIBLE);
			}
		};
		if(isStartAnim){
			Animation animation = AnimationUtils.loadAnimation(BaseApplication.getInstance(), android.R.anim.fade_out);
			animation.setAnimationListener(new AnimationListener() {
				
				@Override
				public void onAnimationStart(Animation animation) {}
				
				@Override
				public void onAnimationRepeat(Animation animation) {}
				
				@Override
				public void onAnimationEnd(Animation animation) {
					view.post(runnable);
				}
			});
			animation.setFillBefore(true);
			view.startAnimation(animation);
		}else{
			runnable.run();
		}
	}
}
