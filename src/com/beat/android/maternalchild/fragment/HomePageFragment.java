package com.beat.android.maternalchild.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.GridLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.beat.android.maternalchild.MainActivity;
import com.beat.android.maternalchild.R;
import com.beat.android.maternalchild.activity.ProductDetailActivity;
import com.beat.android.maternalchild.base.BaseFragment;
import com.beat.android.maternalchild.presenter.TestPresenter;
import com.beat.android.maternalchild.widget.AutoScrollViewPager;
import com.beat.android.maternalchild.widget.CirclePageIndicator;
import com.beat.android.maternalchild.widget.ImagePagerAdapter;
import com.beat.android.maternalchild.widget.PullDownScrollView;
import com.beat.android.maternalchild.widget.PullDownScrollView.RefreshListener;
import com.beat.android.maternalchild.widget.PullDownScrollView.SVOnScrollChangeListener;

public class HomePageFragment extends BaseFragment{

	private AutoScrollViewPager mViewPager;
	private CirclePageIndicator mIndicator;
	private List<String> mNewsestList = new ArrayList<String>();
	private List<String> mSalesList = new ArrayList<String>();
	private int mHeaderHeight;
	private View mHeaderLayout;
	private PullDownScrollView mScrollView;

	@Override
	protected View setContentView() {
		TestPresenter.loadData(getActivity());
		return LayoutInflater.from(getActivity()).inflate(R.layout.home_page_fragment_layout, null);
	}

	@Override
	protected void setView() {
		checkNetWrok();
		initMainActionBar();
		initScrollView();
		initPageScrollView();
		for(int i = 0; i < 4; i++){
			mNewsestList.add("item"+i);
		}
		initNewsetProduct();
		for(int i = 0; i < 6; i++){
			mSalesList.add("sale item"+i);
		}
		initSalesPromotion();
	}
	
	private void initMainActionBar(){
		try {
			mHeaderLayout = ((MainActivity)getActivity()).mHeaderLayout;
			mHeaderLayout.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				
				@Override
				public void onGlobalLayout() {
					mHeaderHeight = mHeaderLayout.getHeight();
					View blankView = new View(getActivity());
					blankView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mHeaderHeight));
					mScrollView.addTopHeaderView(blankView);
					mHeaderLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void initScrollView(){
		mScrollView = (PullDownScrollView)getActivity().findViewById(R.id.pulldown_srollview);
		mScrollView.setOnScrollChangedListener(new SVOnScrollChangeListener() {
			
			@Override
			public void onScrollChanged(int x, int y, int oldx, int oldy) {
				if(y > 60)
					return;
				
				AlphaAnimation anim = new AlphaAnimation((float)(100-oldy)/100, (float)(100-y)/100);
				anim.setDuration(1);
				anim.setFillAfter(true);
				mHeaderLayout.clearAnimation();
				mHeaderLayout.startAnimation(anim);
			}
		});
		mScrollView.setRefreshListener(new RefreshListener() {
			
			@Override
			public void onRefresh() {
				mScrollView.postDelayed(new Runnable() {
					
					@Override
					public void run() {
						mScrollView.stopRefresh();
					}
				}, 3000);
			}
		});
	}
	
	private void initPageScrollView(){
		mViewPager = (AutoScrollViewPager)getView().findViewById(R.id.view_pager);
        mIndicator = (CirclePageIndicator)getView().findViewById(R.id.indicator);

        List<Integer> imageIdList = new ArrayList<Integer>();
        imageIdList.add(R.drawable.banner1);
        imageIdList.add(R.drawable.banner2);
        imageIdList.add(R.drawable.banner3);
        imageIdList.add(R.drawable.banner4);
        mViewPager.setAdapter(new ImagePagerAdapter(getActivity(), imageIdList));
        mIndicator.setViewPager(mViewPager);

        mViewPager.setInterval(3000);
        mViewPager.startAutoScroll();
        mViewPager.setSlideBorderMode(AutoScrollViewPager.SLIDE_BORDER_MODE_TO_PARENT);
//        mViewPager.setCurrentItem(Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % imageIdList.size());
	}
	
	private View getVerticalLineView(){
		View verticalLine = new View(getActivity());
		verticalLine.setLayoutParams(new LinearLayout.LayoutParams(1, LinearLayout.LayoutParams.MATCH_PARENT));
		verticalLine.setBackgroundColor(getActivity().getResources().getColor(R.color.line_color_1));
		return verticalLine;
	}
	
	private View getHorizontalLine(){
		View horizontalLine = new View(getActivity());
		horizontalLine.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1));
		horizontalLine.setBackgroundColor(getActivity().getResources().getColor(R.color.line_color_1));
		return horizontalLine;
	}
	
	private View getNewestItemView(){
		View newestItemView = LayoutInflater.from(getActivity()).inflate(R.layout.newest_product_item, null);
		newestItemView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
				getActivity().startActivity(intent);
			}
		});
		return newestItemView;
	}
	
	private View getSalesPromotionView(){
		View salesPromotionView = LayoutInflater.from(getActivity()).inflate(R.layout.sales_promition_item, null);
		return salesPromotionView;
	}
	
	private void initNewsetProduct(){
		
		LinearLayout newestContentLayout = (LinearLayout)getView().findViewById(R.id.newest_content);
		newestContentLayout.removeAllViews();
		LinearLayout rowLayout = new LinearLayout(getActivity());
		rowLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
		params.weight = 1;
		for(int i = 0; i < mNewsestList.size(); i++){
			if(i%2 == 0){
				rowLayout = new LinearLayout(getActivity());
				rowLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
				rowLayout.addView(getNewestItemView(),params);
				rowLayout.addView(getVerticalLineView());
				if(i == mNewsestList.size()-1){
					rowLayout.addView(new View(getActivity()), params);
					newestContentLayout.addView(rowLayout);
					newestContentLayout.addView(getHorizontalLine());
				}
			}else if(i%2 == 1){
				rowLayout.addView(getNewestItemView(),params);
				newestContentLayout.addView(rowLayout);
				newestContentLayout.addView(getHorizontalLine());
			}
		}
		newestContentLayout.removeViewAt(newestContentLayout.getChildCount()-1);
	}
	
	private void initSalesPromotion(){
		LinearLayout salesPromotionContentLayout = (LinearLayout)getView().findViewById(R.id.sales_promotion_content);
		salesPromotionContentLayout.removeAllViews();
		LinearLayout rowLayout = new LinearLayout(getActivity());
		rowLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
		params.weight = 1;
		for(int i = 0; i < mSalesList.size(); i++){
			if(i%3 == 0){
				rowLayout = new LinearLayout(getActivity());
				rowLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
				rowLayout.addView(getSalesPromotionView(),params);
				rowLayout.addView(getVerticalLineView());
				if(i == mSalesList.size()-1){
					rowLayout.addView(new View(getActivity()), params);
					rowLayout.addView(getVerticalLineView());
					rowLayout.addView(new View(getActivity()), params);
					salesPromotionContentLayout.addView(rowLayout);
					salesPromotionContentLayout.addView(getHorizontalLine());
				}
			}else if(i%3 == 2){
				rowLayout.addView(getSalesPromotionView(),params);
				salesPromotionContentLayout.addView(rowLayout);
				salesPromotionContentLayout.addView(getHorizontalLine());
			}else{
				rowLayout.addView(getSalesPromotionView(),params);
				rowLayout.addView(getVerticalLineView());
				if(i == mSalesList.size()-1){
					rowLayout.addView(new View(getActivity()), params);
					salesPromotionContentLayout.addView(rowLayout);
					salesPromotionContentLayout.addView(getHorizontalLine());
				}
			}
		}
		salesPromotionContentLayout.removeViewAt(salesPromotionContentLayout.getChildCount()-1);
	}

	@Override
	protected void setValue() {
		
	}

	@Override
	protected void setListener() {
		
	}
}
