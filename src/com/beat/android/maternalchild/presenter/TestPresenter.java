package com.beat.android.maternalchild.presenter;

import java.util.List;

import com.beat.android.maternalchild.entity.ProvinceEntity;
import com.beat.android.maternalchild.net.DataJsonParser;
import com.beat.android.maternalchild.net.exception.ResultCodeException;
import com.beat.android.maternalchild.net.exception.ServerErrException;
import com.beat.android.maternalchild.os.TerminableThreadPool;
import com.beat.android.maternalchild.utils.LogUtil;

import android.content.Context;

public class TestPresenter {
	public static void loadData(final Context context){
		Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				try {
					List<ProvinceEntity> data = DataJsonParser.getInstance(context).queryProvinces();
					for (ProvinceEntity provinceEntity : data) {
						LogUtil.e("zlq", provinceEntity.toString());
					}
				} catch (ServerErrException e) {
					e.printStackTrace();
				} catch (ResultCodeException e) {
					e.printStackTrace();
				}
			}
		};
		
		new TerminableThreadPool(runnable).start();
	}
}
