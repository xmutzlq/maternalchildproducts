package com.beat.android.maternalchild.activity;

import com.beat.android.maternalchild.R;
import com.beat.android.maternalchild.base.BaseActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

public class ProductDetailActivity extends BaseActivity {

	@Override
	protected View newContentView(Bundle savedInstanceState) {
		return LayoutInflater.from(ProductDetailActivity.this).inflate(R.layout.product_detail_layout, null);
	}
	
	@Override
	protected void initVar() {
		setTitleContent("产品列表");
	}

}
