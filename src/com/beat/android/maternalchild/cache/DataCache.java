package com.beat.android.maternalchild.cache;

public class DataCache {
	private static DataCache instance;
	
	private String phoneNumber;
	
	public static DataCache getInstance() {
		if (instance == null) {
			instance = new DataCache();
		}
		return instance;
	}
	
	public DataCache(){
		
	}
	
	public void setPhoneNum(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNum(){
		return phoneNumber;
	}
}
