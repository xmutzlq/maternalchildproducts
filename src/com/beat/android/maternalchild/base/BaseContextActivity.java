package com.beat.android.maternalchild.base;

import java.util.HashMap;
import java.util.Map.Entry;

import com.beat.android.maternalchild.base.listener.IActivityObservable;
import com.beat.android.maternalchild.base.listener.IActivityObserver;
import com.beat.android.maternalchild.base.listener.IAppContextObservable;
import com.beat.android.maternalchild.base.listener.IAppContextObserver;
import com.beat.android.maternalchild.base.listener.ITitleBar;
import com.beat.android.maternalchild.common.AppBroadcast;
import com.beat.android.maternalchild.common.ClientInfoUtil;
import com.beat.android.maternalchild.utils.NetWorkUtil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class BaseContextActivity extends AbsContextActivity implements IAppContextObserver, IAppContextObservable, IActivityObservable, ITitleBar{
	private static boolean isInitApp = false;
	
	protected BaseContextActivity this_ = this;
	
	private HashMap<String, BroadcastReceiver> mBroadcastReceivers = new HashMap<String, BroadcastReceiver>();
	private NetWorkListener mNetWorkListener;
	private AppContextObservable mAppContextObservable;
	private ActivityObservable mActivityObservable;
	private Boolean isNetAvailable;
	private boolean isFirst;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		isFirst = true;
		mNetWorkListener = new NetWorkListener();
		mAppContextObservable = new AppContextObservable();
		mActivityObservable = new ActivityObservable();
		initBroadcastReceiver();
		registerContextObservable(this);
		isNetAvailable = NetWorkUtil.isNetAvailable(this_);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}
	
	/**
	 * 退出APP
	 */
	private static void exitApp(){
		if(isInitApp){
			isInitApp = false;
			new Thread() {
				@Override
				public void run() {
					ActivityManage.finishAll();
				}
			}.start();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterAllBroadcastReceiver();
		mAppContextObservable.release();
		mActivityObservable.release();
	}

	@Override
	protected void onUserLeaveHint() {
		super.onUserLeaveHint();
	}

	@Override
	protected void onResume() {
		super.onResume();
		tryDispatchNetworkChange(NetWorkUtil.isNetAvailable(this_));
		registerBroadcastReceiver(android.net.ConnectivityManager.CONNECTIVITY_ACTION, mNetWorkListener);
		dispatchActivityResume(isFirst);
		if(isFirst){
			isFirst = false;
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		unregisterBroadcastReceiver(android.net.ConnectivityManager.CONNECTIVITY_ACTION);
		isNetAvailable = NetWorkUtil.isNetAvailable(this_);
		dispatchActivityPause();
	}

	@Override
	public Object getSystemService(String name) {
		if(Context.TELEPHONY_SERVICE.equals(name)){
			return BaseApplication.getInstance().getTelephonyManager();
		}
		return super.getSystemService(name);
	}
	
	protected void dispatchExitApp(){
		onExitApp();
		exitApp();
	}
	
	@Override
	public void setLeftButton(String tip, int icon, int bgRes) {
		//由子类完成TitleBar的逻辑
	}
	@Override
	public void setRightButton(String tip, int icon, int bgRes) {
		//由子类完成TitleBar的逻辑
	}
	@Override
	public void setLeftButtonEnabled(boolean isEnabled) {
		//由子类完成TitleBar的逻辑
	}
	@Override
	public void setRightButtonEnabled(boolean isEnabled) {
		//由子类完成TitleBar的逻辑
	}
	@Override
	public void resetTitleBar() {
		//由子类完成TitleBar的逻辑
	}
	@Override
	public void setTitleView(View view) {
		//由子类完成TitleBar的逻辑
	}
	@Override
	public void setTitleContent(String titleStr) {
		//由子类完成TitleBar的逻辑
	}
	@Override
	public void removeTitleView(View view) {
		//删除一个添加到Title的View
	}
	
	protected void onExitApp() {
		
	}
	
	@Override
	public void onLanguageChange(){
		
	}
	
	@Override
	public void onNetworkChange(boolean isAvailable) {
		
	}
	
	@Override
	public void onLoadTheme() {
		
	}
	
	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		
	}
	
	@Override
	public void dispatchLanguageChange() {
		mAppContextObservable.dispatchLanguageChange();
	}
	
	@Override
	public void dispatchLoadTheme(){
		mAppContextObservable.dispatchLoadTheme();
	}
	
	@Override
	public void dispatchUserLoginStateChange(boolean isLogin){
		mAppContextObservable.dispatchUserLoginStateChange(isLogin);
	}
	
	private void tryDispatchNetworkChange(boolean isAvailable){
		if(isNetAvailable == null || isNetAvailable != isAvailable){
			isNetAvailable = isAvailable;
			dispatchContextNetworkChange(isAvailable);
		}
	}
	
	@Override
	public void dispatchContextNetworkChange(boolean isAvailable) {
		mAppContextObservable.dispatchContextNetworkChange(isAvailable);
	}
	
	@Override
	public void registerContextObservable(IAppContextObserver contextCallBack){
		mAppContextObservable.registerContextObservable(contextCallBack);
	}
	
	@Override
	public void unregisterContextObservable(IAppContextObserver contextCallBack){
		mAppContextObservable.unregisterContextObservable(contextCallBack);
	}
	
	@Override
	public void registerActivityObserver(IActivityObserver observer) {
		mActivityObservable.registerActivityObserver(observer);
	}

	@Override
	public void unregisterActivityObserver(IActivityObserver observer) {
		mActivityObservable.unregisterActivityObserver(observer);
	}

	@Override
	public boolean dispatchMenuOpened(int featureId, Menu menu) {
		return mActivityObservable.dispatchMenuOpened(featureId, menu);
	}

	@Override
	public boolean dispatchOptionsItemSelected(MenuItem item) {
		return mActivityObservable.dispatchOptionsItemSelected(item);
	}
	
	@Override
	public boolean dispatchActivityResult(int requestCode, int resultCode, Intent data){
		return mActivityObservable.dispatchActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void dispatchActivityResume(boolean isFirst) {
		mActivityObservable.dispatchActivityResume(isFirst);
	}
	
	@Override
	public void dispatchActivityPause() {
		mActivityObservable.dispatchActivityPause();
	}

	@Override
	public boolean dispatchBackPressed() {
		return mActivityObservable.dispatchBackPressed();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		dispatchActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK){
			if(!dispatchBackPressed()){
				return super.onKeyUp(keyCode, event);
			}else{
				return true;
			}
        }
		return super.onKeyUp(keyCode, event);
	}
	
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		if(!dispatchMenuOpened(featureId, menu)){
			return super.onMenuOpened(featureId, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(!dispatchOptionsItemSelected(item)){
			return super.onOptionsItemSelected(item);
		}
		return true;
	}
	
	private void initBroadcastReceiver(){
		mBroadcastReceivers.put(AppBroadcast.ACTION_CLOSE_APP, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dispatchExitApp();
			}
		});
		mBroadcastReceivers.put(AppBroadcast.ACTION_USER_LOGIN_STATE_CHANGE, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dispatchUserLoginStateChange(!ClientInfoUtil.isGuest());
			}
		});
		mBroadcastReceivers.put(AppBroadcast.ACTION_THEME_CHANGE, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dispatchLoadTheme();
			}
		});
		mBroadcastReceivers.put(AppBroadcast.ACTION_LANGUAGE_CHANGE, new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dispatchLanguageChange();
			}
		});
		registerAllBroadcastReceiver();
	}
	
	private void registerBroadcastReceiver(String action,BroadcastReceiver receiver){
		BroadcastReceiver oldReceiver = mBroadcastReceivers.get(action);
		if(oldReceiver == null){
			mBroadcastReceivers.put(action, receiver);
			super.registerReceiver(receiver, new IntentFilter(action));
		}
	}

	private void unregisterBroadcastReceiver(String action){
		BroadcastReceiver receiver = mBroadcastReceivers.get(action);
		if(receiver != null){
			mBroadcastReceivers.remove(action);
			super.unregisterReceiver(receiver);
		}
	}
	
	private void registerAllBroadcastReceiver(){
		for(Entry<String, BroadcastReceiver> entry : mBroadcastReceivers.entrySet()){
			super.registerReceiver(entry.getValue(), new IntentFilter(entry.getKey()));
		}
	}

	private void unregisterAllBroadcastReceiver(){
		for(BroadcastReceiver receiver: mBroadcastReceivers.values()){
			super.unregisterReceiver(receiver);
		}
		mBroadcastReceivers.clear();
	}
	
	private class NetWorkListener extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			tryDispatchNetworkChange(NetWorkUtil.isNetAvailable(this_));
		}  
	}
}
