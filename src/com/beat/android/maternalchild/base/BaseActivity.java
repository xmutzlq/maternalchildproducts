package com.beat.android.maternalchild.base;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

import com.beat.android.maternalchild.R;
import com.beat.android.maternalchild.base.listener.ITitleBar;
import com.beat.android.maternalchild.common.controller.ILoadAndRetryController;
import com.beat.android.maternalchild.common.controller.INetViewBuilder;
import com.beat.android.maternalchild.common.controller.LoadAndRetryController;
import com.beat.android.maternalchild.common.theme.Theme;
import com.beat.android.maternalchild.common.theme.ThemeController;
import com.beat.android.maternalchild.os.INetAsyncTask;
import com.beat.android.maternalchild.utils.DimensionsUtil;
import com.beat.android.maternalchild.widget.ViewAnimDecorator;

public abstract class BaseActivity extends BaseContextActivity implements ILoadAndRetryController{
	private View mLoadingLayout;
	private Theme theme;
	protected FrameLayout contentFrameLayout;
	private LoadAndRetryController mLoadAndRetryController;
	private String[] mWisdomArrStr;
	private ViewGroup mRootView;
	private ViewGroup mFullScreenLay;
	private ViewGroup mTitleCenterLay;
	private FrameLayout mLeftBut;
	private View mRightBut;
	private TextView mLeftButTV;
	private TextView mRightButTV;
	private ImageView mLeftButIV;
	private ImageView mRightButIV;
//	private View mLeftLine;
//	private View mRightLine;
	private TextView mTitleTV;
	private ViewGroup mForeground;
	private View mLoadingView;
	private View mNetRetryLay;
	private ImageView mNetRetryTipIv;
	private Button mNetRetryBut;
	private TextView mNetRetryTipTv;
	private TextView mLoadingTv;
	private FrameLayout mLeftButOne;
	private TextView mLeftButOneTV;
	private ImageView mLeftButOneIV;
	private View mRightButOne;
	private TextView mRightButOneTV;
	private ImageView mRightButOneIV;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		resetView(savedInstanceState);
		initVar();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onUserLoginStateChange(boolean isLogin) {
		super.onUserLoginStateChange(isLogin);
	}

	protected boolean isTitleBackBtnEnabled(){
		return true;
	}
	
	protected boolean needLoadView(){
		return true;
	}
	
	/**
	 * 顶部是否需要自定义
	 * @return false 自定义
	 */
	protected boolean headerEnable(){
		return true;
	}
	
	@Override
	public void setContentView(int layoutResID) {
		setContentView(getLayoutInflater().inflate(layoutResID, null));
	}

	@Override
	public void setContentView(View view) {
		setContentView(view,null);
	}
	
	@Override
	public void setContentView(View view,android.view.ViewGroup.LayoutParams params) {
		contentFrameLayout.addView(view,params);
	}

	protected void resetView(Bundle savedInstanceState){
		mWisdomArrStr = getResources().getStringArray(R.array.wisdom_arr_str);
		super.setContentView(R.layout.common_main);
		mFullScreenLay = (ViewGroup) findViewById(R.id.full_screen_lay);
		mRootView = (ViewGroup) findViewById(R.id.root_view);
		contentFrameLayout = (FrameLayout) findViewById(R.id.activity_content_lay);
		View newView = newContentView(savedInstanceState);
		if (newView != null) {
			contentFrameLayout.addView(newView);
		}
		mForeground = (ViewGroup) findViewById(R.id.activity_foreground_lay);
		mTitleCenterLay = (ViewGroup) findViewById(R.id.body_title_center_lay);
		mTitleTV = (TextView) findViewById(R.id.body_title);
		
		resetNaviButton();
		
		String titleStr = getContentTitle();
		if(!TextUtils.isEmpty(titleStr)){
			mTitleTV.setText(titleStr);
		}
		
		setThemeStyle();
		mLoadingLayout = getLayoutInflater().inflate(R.layout.load_and_retry_lay, null);
		
		if(needLoadView()){
			LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.MATCH_PARENT);
			params.addRule(RelativeLayout.BELOW, R.id.header_lay);
			mRootView.addView(mLoadingLayout, params);
		}
		
		mLoadingView = mLoadingLayout.findViewById(R.id.loading_lay);
		mLoadingTv = (TextView)mLoadingLayout.findViewById(R.id.loading_tv);
		mNetRetryLay = mLoadingLayout.findViewById(R.id.net_retry_lay);
		mNetRetryTipIv = (ImageView)mLoadingLayout.findViewById(R.id.net_retry_tip_iv);
		mNetRetryTipTv = (TextView)mLoadingLayout.findViewById(R.id.net_retry_tip_tv);
		mNetRetryBut = (Button)mLoadingLayout.findViewById(R.id.net_retry_but);

		mLoadAndRetryController = new LoadAndRetryController(this, new INetViewBuilder() {
			@Override
			public void onShowRetryView(OnClickListener retryListener) {
				BaseActivity.this.onShowRetryView(retryListener);
			}
			
			@Override
			public void onShowNetSettingView(OnClickListener netSettingListener,
					OnClickListener gotoBookShelfListener) {
				BaseActivity.this.onShowNetSettingView(netSettingListener, gotoBookShelfListener);
			}
			
			@Override
			public void onShowLoadingView() {
				BaseActivity.this.onShowLoadingView();
			}

			@Override
			public void onHideLoadAndRetryView(int lastLoadViewType) {
				BaseActivity.this.onHideLoadAndRetryView(lastLoadViewType);
			}
		});
		resetTitleBar();
	}
	
	

	public void setForegroundView(View child){
		mForeground.removeAllViews();
		mForeground.addView(child);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(!super.onOptionsItemSelected(item) 
				&& ITitleBar.MENU_ITEM_ID_LEFT_BUTTON == item.getItemId()
				&& Integer.valueOf(R.drawable.back_btn).equals(mLeftButIV.getTag())){
			if(!onClickBackBtn()){
				finish();
			}
		}
		return false;
	}

	protected boolean onClickBackBtn(){
		return false;
	}
	
	protected void addFullScreenView(View view){
		if(view != null && view.getParent() == null){
			mFullScreenLay.addView(view,new LayoutParams(
					android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.MATCH_PARENT));
		}
	}
	
	protected void removeFullScreenView(View view){
		if(view != null && mFullScreenLay.equals(view.getParent())){
			mFullScreenLay.removeView(view);
		}
	}

	@Override
	public void removeTitleView(View view) {
		super.removeTitleView(view);
		if(view != null && view.getParent() == mTitleCenterLay){
			mTitleCenterLay.removeView(view);
		}
	}
	
	@Override
	public void setTitleView(View view) {
		super.setTitleView(view);
		if(view != null && view.getParent() == null){
			mTitleCenterLay.removeAllViews();
			mTitleCenterLay.addView(view);
		}
	}

	@Override
	public void setTitleContent(String titleStr){
		super.setTitleContent(titleStr);
		if(titleStr != null) {
			mTitleTV.setText(titleStr);
		}
		if(!TextUtils.isEmpty(titleStr)){
			if(!mTitleCenterLay.equals(mTitleTV.getParent())){
				mTitleCenterLay.removeAllViews();
				mTitleCenterLay.addView(mTitleTV);
			}
		}
	}
	
	protected void resetNaviButton(){
		mLeftBut = (FrameLayout)findViewById(R.id.body_title_left_but);
		mLeftButTV = (TextView) findViewById(R.id.body_title_left_but_tv);
		mLeftButIV = (ImageView) findViewById(R.id.body_title_left_but_iv);
//		mLeftLine = findViewById(R.id.body_title_left_line);
		mRightBut = findViewById(R.id.body_title_right_but);
		mRightButTV = (TextView) findViewById(R.id.body_title_right_but_tv);
		mRightButIV = (ImageView) findViewById(R.id.body_title_right_but_iv);
//		mRightLine = findViewById(R.id.body_title_right_line);
		
		mLeftBut.setOnClickListener(new OnClickListener() {
			MenuItem item;
			@Override
			public void onClick(View v) {
				if(item == null){
					item = new SimpleMenuItem(ITitleBar.MENU_ITEM_ID_LEFT_BUTTON);
				}
				onOptionsItemSelected(item);
			}
		});
		
		mLeftButOne = (FrameLayout)findViewById(R.id.body_title_left_but_one);
		mLeftButOneTV = (TextView) findViewById(R.id.body_title_left_but_one_tv);
		mLeftButOneIV = (ImageView) findViewById(R.id.body_title_left_but_one_iv);
		
		mRightButOne = findViewById(R.id.body_title_right_but_one);
		mRightButOneTV = (TextView) findViewById(R.id.body_title_right_but_one_tv);
		mRightButOneIV = (ImageView) findViewById(R.id.body_title_right_but_one_iv);
	}
	
	private void resetLeftButton() {
		if(mLeftButIV.getParent() == null) {
			FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(DimensionsUtil.DIPToPX(50), android.widget.FrameLayout.LayoutParams.MATCH_PARENT);
			mLeftBut.addView(mLeftButIV, lp);
			mLeftButIV.setTag(null);
		}
		
		if(mLeftButTV.getParent() == null) {
			FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(DimensionsUtil.DIPToPX(50), android.widget.FrameLayout.LayoutParams.MATCH_PARENT);
			mLeftBut.addView(mLeftButTV, lp);
		}
		android.widget.RelativeLayout.LayoutParams layoutParams
		= new android.widget.RelativeLayout.LayoutParams(android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT, 
				android.widget.RelativeLayout.LayoutParams.MATCH_PARENT);
		
		layoutParams.alignWithParent = true;
		layoutParams.leftMargin = DimensionsUtil.DIPToPX(3);
		layoutParams.rightMargin = DimensionsUtil.DIPToPX(3);
		layoutParams.topMargin = DimensionsUtil.DIPToPX(3);
		layoutParams.bottomMargin = DimensionsUtil.DIPToPX(3);
		layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
		mLeftBut.setLayoutParams(layoutParams);
		mLeftBut.setPadding(0, 0, 0, 0);
		mLeftBut.setBackgroundResource(R.drawable.title_bar_but_selector);
	}
	
	public void setLeftButton(String tip, int icon, int bgRes, View.OnClickListener listener) {
		setLeftButtonEnabled(true);
		if(tip == null){
			tip = "";
		}
		
		mLeftButTV.setText(tip);
		
		if(icon > 0){
			mLeftButIV.setImageResource(icon);
		}else{
			mLeftButIV.setImageBitmap(null);
		}
		
		if(bgRes > 0){
			mLeftBut.setBackgroundResource(bgRes);
//			mLeftLine.setVisibility(View.INVISIBLE);
			
		}
		
		mLeftButIV.setTag(null);
		
		if(listener != null)
			mLeftBut.setOnClickListener(listener);
	}
	
	public void setLeftOneButton(String tip, int icon, int bgRes, View.OnClickListener listener) {
		setLeftOneButtonEnabled(true);
		if(tip == null){
			tip = "";
		}
		
		mLeftButOneTV.setText(tip);
		
		if(icon > 0){
			mLeftButOneIV.setImageResource(icon);
		}else{
			mLeftButOneIV.setImageBitmap(null);
		}
		
		if(bgRes > 0){
			mLeftButOne.setBackgroundResource(bgRes);
//			mLeftLine.setVisibility(View.INVISIBLE);
			
		}
		
		mLeftButOneIV.setTag(null);
		
		if(listener != null)
			mLeftButOne.setOnClickListener(listener);
	}
	
	public void setRightButton(String tip, int icon, int bgRes, View.OnClickListener listener) {
		setRightButtonEnabled(true);
		if(tip == null){
			tip = "";
		}
		
		mRightButTV.setText(tip);
		
		if(icon > 0){
			mRightButIV.setImageResource(icon);
		}else{
			mRightButIV.setImageBitmap(null);
		}
		
		if(bgRes > 0){
			mRightBut.setBackgroundResource(bgRes);
//			mRightLine.setVisibility(View.INVISIBLE);
			
		}
		
		if(listener != null)
			mRightBut.setOnClickListener(listener);
		
		mRightButTV.setTag(null);
	}
	
	public void setRightOneButton(String tip, int icon, int bgRes, View.OnClickListener listener) {
		setRightOneButtonEnabled(true);
		if(tip == null){
			tip = "";
		}
		
		mRightButOneTV.setText(tip);
		
		if(icon > 0){
			mRightButOneIV.setImageResource(icon);
		}else{
			mRightButOneIV.setImageBitmap(null);
		}
		
		if(bgRes > 0){
			mRightButOne.setBackgroundResource(bgRes);
			
		}
		
		if(listener != null)
			mRightButOne.setOnClickListener(listener);
		
		mRightButTV.setTag(null);
	}
	
	public void setLeftButtonEnabled(boolean isEnabled) {
		if(isEnabled){
			mLeftBut.setVisibility(View.VISIBLE);
//			mLeftLine.setVisibility(View.VISIBLE);
		}else{
			mLeftBut.setVisibility(View.INVISIBLE);
//			mLeftLine.setVisibility(View.INVISIBLE);
		}
	}
	
	public void setLeftOneButtonEnabled(boolean isEnabled) {
		if(isEnabled){
			mLeftButOne.setVisibility(View.VISIBLE);
		}else{
			mLeftButOne.setVisibility(View.INVISIBLE);
		}
	}
	
	public void setRightButtonEnabled(boolean isEnabled) {
		if(isEnabled){
			mRightBut.setVisibility(View.VISIBLE);
//			mRightLine.setVisibility(View.VISIBLE);
		}else{
			mRightBut.setVisibility(View.INVISIBLE);
//			mRightLine.setVisibility(View.INVISIBLE);
		}
	}
	
	public void setRightOneButtonEnabled(boolean isEnabled) {
		if(isEnabled){
			mRightButOne.setVisibility(View.VISIBLE);
		}else{
			mRightButOne.setVisibility(View.INVISIBLE);
		}
	}
	
	public void setRightButtonShadowLayer(){
		mRightButTV.setShadowLayer(3, 3.0f, 3.0f, Color.GRAY);  
	}
	
	@Override
	public void resetTitleBar() {
		super.resetTitleBar();
		mTitleCenterLay.removeAllViews();
		mTitleCenterLay.addView(mTitleTV);
		
		resetLeftButton();
		
		setLeftOneButtonEnabled(false);
		setRightButtonEnabled(false);
		setRightOneButtonEnabled(false);
		setRightButton("", 0, 0);
		if(isTitleBackBtnEnabled()){
			setLeftButtonEnabled(true);
			setLeftButton(null, R.drawable.back_btn, 0, null);
			mLeftButIV.setTag(R.drawable.back_btn);
		}else{
			setLeftButtonEnabled(false);
		}
		
		if(!headerEnable()){
			findViewById(R.id.header_lay).setVisibility(View.GONE);
		} else {
			findViewById(R.id.header_lay).setVisibility(View.VISIBLE);
		}
	}
	
	protected abstract View newContentView(Bundle savedInstanceState);
	
	protected String getContentTitle(){
		return null;
	};
	
	protected void initVar(){
	}
	
	private void setThemeStyle() {
		theme = ThemeController.getInstance().getTheme();
		getWindow().setBackgroundDrawableResource(theme.windowBackgroud());
	}

	@Override
	public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
		if (repeatCount >= 1) {
			return true;
		}
		return super.onKeyMultiple(keyCode, repeatCount, event);
	}
	
	/**
	 * 获取右按钮名称
	 * */
	protected String getRightButtonTitle(){
		return mRightButTV.getText().toString();
	}
	
	@Override
	public boolean checkNetWrok(){
		return mLoadAndRetryController.checkNetWrok();
	}
	
	@Override
	public void setNetTack(INetAsyncTask lastTask){
		mLoadAndRetryController.setNetTack(lastTask);
	}
	
	@Override
	public boolean tryStartNetTack(INetAsyncTask lastTask){
		return mLoadAndRetryController.tryStartNetTack(lastTask);
	}
	
	@Override
	public void dispatchNetworkChange(boolean isAvailable) {
		mLoadAndRetryController.dispatchNetworkChange(isAvailable);
	}
	
	@Override
	public void showNetSettingView() {
		mLoadAndRetryController.showNetSettingView();
	}
	
	@Override
	public void showRetryView() {
		mLoadAndRetryController.showRetryView();
	}
	
	@Override
	public void showLoadingView() {
		mLoadAndRetryController.showLoadingView();
	}

	@Override
	public void hideLoadAndRetryView() {
		mLoadAndRetryController.hideLoadAndRetryView();
	}
	
	@Override
	public boolean isTransparentLoadingView() {
		return mLoadAndRetryController.isTransparentLoadingView();
	}
	
	@Override
	public void onNetworkChange(boolean isAvailable) {
		super.onNetworkChange(isAvailable);
		dispatchNetworkChange(isAvailable);
	}
	
	protected void showNoDataView(){
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipIv.setImageResource(R.drawable.sad_face);
		mNetRetryTipTv.setText(R.string.tip_no_data);
		mNetRetryBut.setOnClickListener(null);
		mNetRetryBut.setVisibility(View.GONE);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	protected void showCtmTipView(int imgResId,String tipText,String btnText, OnClickListener clickListener){
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipIv.setImageResource(imgResId);
		mNetRetryTipTv.setText(tipText);
		mNetRetryBut.setText(btnText);
		mNetRetryBut.setOnClickListener(clickListener);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	private boolean setWisdomStr(TextView tipTV){
		if(tipTV == null || mWisdomArrStr == null || mWisdomArrStr.length == 0){
			return false;
		}
		int lastWisdomArrIndex = LoadAndRetryController.mLastWisdomArrIndex;
		lastWisdomArrIndex++;
		if(mWisdomArrStr.length == lastWisdomArrIndex){
			lastWisdomArrIndex = 0;
		}
		tipTV.setText(mWisdomArrStr[lastWisdomArrIndex]);
		LoadAndRetryController.mLastWisdomArrIndex = lastWisdomArrIndex;
		return true;
	}
	
	protected void onShowRetryView(OnClickListener retryListener) {
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipTv.setText(R.string.tip_no_content);
		mNetRetryBut.setText(R.string.btn_text_retry);
		mNetRetryBut.setOnClickListener(retryListener);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	protected void onShowLoadingView() {
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		if(!setWisdomStr(mLoadingTv)){
			mLoadingTv.setText(R.string.waitting_dialog_load_tip);
		}
		mNetRetryLay.setVisibility(View.GONE);
		mLoadingView.setVisibility(View.VISIBLE);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mLoadingView,false);
	}
	
	protected void onShowNetSettingView(OnClickListener netSettingListener,
			OnClickListener gotoBookShelfListener) {
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipIv.setImageResource(R.drawable.unconnection);
		mNetRetryTipTv.setText(R.string.conection_unavailable);
		mNetRetryBut.setText(R.string.setting);
		mNetRetryBut.setOnClickListener(netSettingListener);
		mNetRetryTipIv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//TODO 打开网络帮助界面
			}
		});
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	private void setViewBGColor(View view){
		if(isTransparentLoadingView()){
			view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()) + 0x99000000);
		}else{
			view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()));
		}
	}
	
	protected int getDefaultLoadingViewBGColor(){
		return R.color.window_bg;
	}
	
	protected void onHiedNetSettingView() {
		
	}
	
	protected void onHideLoadAndRetryView(int lastLoadViewType) {
		if(lastLoadViewType == ILoadAndRetryController.LOAD_VIEW_TYPE_SETTING_NET){
			onHiedNetSettingView();
		}
		ViewAnimDecorator.hideView(mLoadingLayout,isViewAnimEnabled());
	}
	
	protected boolean isViewAnimEnabled(){
		return true;
	}
}
