package com.beat.android.maternalchild.base;

import com.beat.android.maternalchild.R;
import com.beat.android.maternalchild.base.listener.IAppContextObserver;
import com.beat.android.maternalchild.common.controller.ILoadAndRetryController;
import com.beat.android.maternalchild.utils.NetWorkUtil;
import com.beat.android.maternalchild.widget.ViewAnimDecorator;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

/**
 * Fragment基类
 * com.beat.android.maternalchild.base.BaseFragment
 * @author zenglq <br/>
 * create at 2016年1月7日 下午5:49:27
 */
public abstract class BaseFragment extends Fragment implements IAppContextObserver {
	protected Context mContext;
	protected View mContentView;
	private View mLoadingLayout;
	private View mLoadingView;
	private TextView mLoadingTv;
	private View mNetRetryLay;
	private ImageView mNetRetryTipIv;
	private TextView mNetRetryTipTv;
	private Button mNetRetryBut;
	private boolean isCheckNetwork;

	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		setView();
		if (isAdded()){
			setValue();
			setListener();
		}
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		mContext = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		if(getActivity() instanceof BaseContextActivity){
			((BaseContextActivity)getActivity()).registerContextObservable(this);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View layoutView = LayoutInflater.from(getActivity()).inflate(R.layout.base_fragment_layout, null);
		ViewGroup rootView = (RelativeLayout)layoutView.findViewById(R.id.root_view);
		initLoadingView(rootView);
		ViewGroup containerView = (ViewGroup)layoutView.findViewById(R.id.main_content);
		mContentView = setContentView();
		
		if(mContentView != null)
			containerView.addView(mContentView);
		
		return layoutView;
	}
	
	public boolean checkNetWrok(){
		isCheckNetwork = true;
		if(!NetWorkUtil.isNetAvailable(mContext)){
			showNetSettingView();
			return false;
		}
		return true;
	}
	
	private void initLoadingView(ViewGroup rootView){
		LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,android.view.ViewGroup.LayoutParams.MATCH_PARENT);
		mLoadingLayout = LayoutInflater.from(getActivity()).inflate(R.layout.load_and_retry_lay, null);
		rootView.addView(mLoadingLayout, params);
		
		mLoadingView = mLoadingLayout.findViewById(R.id.loading_lay);
		mLoadingTv = (TextView)mLoadingLayout.findViewById(R.id.loading_tv);
		mNetRetryLay = mLoadingLayout.findViewById(R.id.net_retry_lay);
		mNetRetryTipIv = (ImageView)mLoadingLayout.findViewById(R.id.net_retry_tip_iv);
		mNetRetryTipTv = (TextView)mLoadingLayout.findViewById(R.id.net_retry_tip_tv);
		mNetRetryBut = (Button)mLoadingLayout.findViewById(R.id.net_retry_but);
	}
	
	protected void showLoadingView() {
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
//		if(!setWisdomStr(mLoadingTv)){
			mLoadingTv.setText(R.string.waitting_dialog_load_tip);
//		}
		mNetRetryLay.setVisibility(View.GONE);
		mLoadingView.setVisibility(View.VISIBLE);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mLoadingView,false);
	}
	
	protected void showRetryView(OnClickListener retryListener) {
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipTv.setText(R.string.tip_no_content);
		mNetRetryBut.setText(R.string.btn_text_retry);
		mNetRetryBut.setOnClickListener(retryListener);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	protected void showNoDataView(){
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipIv.setImageResource(R.drawable.sad_face);
		mNetRetryTipTv.setText(R.string.tip_no_data);
		mNetRetryBut.setOnClickListener(null);
		mNetRetryBut.setVisibility(View.GONE);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	protected void showCtmTipView(int imgResId,String tipText,String btnText, OnClickListener clickListener){
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipIv.setImageResource(imgResId);
		mNetRetryTipTv.setText(tipText);
		mNetRetryBut.setText(btnText);
		mNetRetryBut.setOnClickListener(clickListener);
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	protected void showNetSettingView() {
		mLoadingLayout.setAnimation(null);
		mLoadingLayout.setVisibility(View.VISIBLE);
		mNetRetryLay.setVisibility(View.VISIBLE);
		mLoadingView.setVisibility(View.GONE);
		mNetRetryTipIv.setImageResource(R.drawable.unconnection);
		mNetRetryTipTv.setText(R.string.conection_unavailable);
		mNetRetryBut.setText(R.string.setting);
		mNetRetryBut.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//打开网络帮助界面
				Intent intent = null;
                //判断手机系统的版本  即API大于10 就是3.0或以上版本 
                if(android.os.Build.VERSION.SDK_INT>10){
                    intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
                }else{
                    intent = new Intent();
                    ComponentName component = new ComponentName("com.android.settings","com.android.settings.WirelessSettings");
                    intent.setComponent(component);
                    intent.setAction("android.intent.action.VIEW");
                }
                startActivity(intent);
			}
		});
		setViewBGColor(mLoadingLayout);
		ViewAnimDecorator.showView(mNetRetryLay,false);
	}
	
	private void setViewBGColor(View view){
//		if(isTransparentLoadingView()){
//			view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()) + 0x99000000);
//		}else{
//			view.setBackgroundColor(getResources().getColor(getDefaultLoadingViewBGColor()));
//		}
	}
	
	protected int getDefaultLoadingViewBGColor(){
		return R.color.window_bg;
	}
	
	protected void onHiedNetSettingView() {
		
	}
	
	protected void hideLoadAndRetryView(int lastLoadViewType) {
		if(lastLoadViewType == ILoadAndRetryController.LOAD_VIEW_TYPE_SETTING_NET){
			onHiedNetSettingView();
		}
		ViewAnimDecorator.hideView(mLoadingLayout,isViewAnimEnabled());
	}
	
	protected boolean isViewAnimEnabled(){
		return true;
	}

	/**
	 * @return
	 * @description: 设置显示页面的视图内容
	 */
	protected abstract View setContentView();

	/**
	 * 
	 * @description: 初始化页面
	 */
	protected abstract void setView();

	/**
	 * 
	 * @description: 初始化数据
	 */
	protected abstract void setValue();

	/**
	 * 
	 * @description: 初始化监听器
	 */
	protected abstract void setListener();
	
	@Override
	public void onLoadTheme() {
	}
	
	@Override
	public void onUserLoginStateChange(boolean isLogin) {
	}
	
	@Override
	public void onNetworkChange(boolean isAvailable) {
		if(!isCheckNetwork)
			return;
		
		if(!isAvailable){
			showNetSettingView();
		}else{
			hideLoadAndRetryView(-1);
		}
	}
	
	@Override
	public void onLanguageChange() {
	}
}
