package com.beat.android.maternalchild.base;



import com.beat.android.maternalchild.utils.CrashCatchHandler;
import com.beat.android.maternalchild.utils.LogUtil;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

public abstract class BaseApplication extends Application{
	
	private static Handler mHandler;
	
	private TelephonyManager mTelephonyManager;
	
	private static BaseApplication instance;
	
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		//处理程序崩溃日志记录
		catchError();
		//处理程序日志
		dealLog();
		mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);  
	}
	
	public static BaseApplication getInstance(){
		return instance;
	}
	
	public static Handler getHandler(){
		if(mHandler == null){
			mHandler = new Handler(Looper.getMainLooper());
		}
		return mHandler;
	}
	
	public TelephonyManager getTelephonyManager(){
		return mTelephonyManager;
	}
	
	/**
	 *  异常扑捉的处理，记录日志
	 */
	private void catchError() {
		String fileDir = getAppSdcardDir();
		if(!TextUtils.isEmpty(fileDir)){
			new CrashCatchHandler(fileDir);
		}
	}
	
	/** 
	 * 设置日志的输出到SDCARD和是否在LOGCAT打印
	 */
	private void dealLog(){
		String TAG = getPackageName();
		String logFile = "";
		if (isWriteLogToSdcard()) {
			logFile = getAppSdcardDir() + "log.txt";
		}
		LogUtil.init(TAG, isDebug(), logFile);
	}
	
	/** 设置程序在SDCARD的目录
	 * @return
	 */
	protected abstract String getAppSdcardDir();
	
	/** 设置是否日志写入SDCARD
	 * @return
	 */
	protected abstract boolean isWriteLogToSdcard();
	
	/** 设置是否DEBUG模式
	 * @return
	 */
	protected abstract boolean isDebug();
}
