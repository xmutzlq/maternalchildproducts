package com.beat.android.maternalchild.base.listener;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;


public interface IActivityObservable {
	/**
	 * ע��
	 * @param contextCallBack
	 */
	public void registerActivityObserver(IActivityObserver observer);
	/**
	 * ע��
	 * @param contextCallBack
	 */
	public void unregisterActivityObserver(IActivityObserver observer);
	/**
	 * MenuOpened
	 */
	public boolean dispatchMenuOpened(int featureId, Menu menu);
	/**
	 * OptionsItemSelected
	 */
	public boolean dispatchOptionsItemSelected(MenuItem item);
	/**
	 * onResume
	 */
	public void dispatchActivityResume(boolean isFirst);
	/**
	 * onPause
	 */
	public void dispatchActivityPause();
	/**
	 * ���ؼ�
	 */
	public boolean dispatchBackPressed();
	/**
	 * onActivityResult
	 * @param requestCode
	 * @param resultCode
	 * @param data
	 */
	public boolean dispatchActivityResult(int requestCode, int resultCode, Intent data);
}
