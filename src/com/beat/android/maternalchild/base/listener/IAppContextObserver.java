package com.beat.android.maternalchild.base.listener;


/**
 * com.beat.android.maternalchild.base.listener.IAppContextObserver
 * @author zenglq <br/>
 * create at 2016��1��7�� ����3:36:46
 */
public interface IAppContextObserver {
	/**
	 * ��������
	 */
	public void onLoadTheme();
	/**
	 * ������¼״̬�ı�
	 * @param isLogin
	 */
	public void onUserLoginStateChange(boolean isLogin);
	/**
	 * ��������״̬�ı�
	 * @param isAvailable
	 */
	public void onNetworkChange(boolean isAvailable);
	/**
	 * �������Ըı�
	 */
	public void onLanguageChange();
}
