package com.beat.android.maternalchild.base.listener;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

public interface IActivityObserver {
	
	/**
	 * @param featureId
	 * @param menu
	 * @return
	 */
	public boolean onMenuOpened(int featureId, Menu menu);
	/**
	 * @param item
	 * @return
	 */
	public boolean onOptionsItemSelected(MenuItem item);
	/**
	 * onActivityPause
	 */
	public void onActivityPause();
	/**
	 * onActivityResume
	 * @param isFirst
	 */
	public void onActivityResume(boolean isFirst);
	/**
	 * onBackPressed
	 * @return
	 */
	public boolean onBackPressed();
	/**
	 * onActivityResult
	 * @param requestCode
	 * @param resultCode
	 * @param data
	 * @return
	 */
	public boolean onActivityResult(int requestCode, int resultCode, Intent data);
}
