package com.beat.android.maternalchild.base.listener;


/**
 * com.beat.android.maternalchild.base.listener.IAppContextObservable
 * @author zenglq <br/>
 * create at 2016��1��7�� ����3:37:39
 */
public interface IAppContextObservable {
	/**
	 * ע��
	 * @param contextCallBack
	 */
	public void registerContextObservable(IAppContextObserver contextCallBack);
	/**
	 * ע��
	 * @param contextCallBack
	 */
	public void unregisterContextObservable(IAppContextObserver contextCallBack);
	/**
	 * ��ǲ��������
	 */
	public void dispatchLoadTheme();
	/**
	 * ��ǲ�����û���¼״̬
	 */
	public void dispatchUserLoginStateChange(boolean isLogin);
	/**
	 * ��ǲ��������״̬
	 */
	public void dispatchContextNetworkChange(boolean isAvailable);
	/**
	 * ��ǲ�������Ըı�
	 */
	public void dispatchLanguageChange();
}
