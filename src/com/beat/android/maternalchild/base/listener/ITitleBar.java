package com.beat.android.maternalchild.base.listener;

import android.view.View;

/**
 * 顶部标题设置
 * com.beat.android.maternalchild.base.listener.ITitleBar
 * @author zenglq <br/>
 * create at 2016年1月7日 下午3:33:14
 */
public interface ITitleBar {
	public static final int MENU_ITEM_ID_LEFT_BUTTON = "MENU_ITEM_ID_LEFT_BUTTON".hashCode();
	public static final int MENU_ITEM_ID_RIGHT_BUTTON = "MENU_ITEM_ID_RIGHT_BUTTON".hashCode();
	/**
	 * 设置TitleView
	 * @param view
	 */
	public void setTitleView(View view);
	/**
	 * 去除TitleView
	 * @param view
	 */
	public void removeTitleView(View view);
	/**
	 * 设置标题内容
	 * @param titleStr
	 */
	public void setTitleContent(String titleStr);
	/**
	 * 设置左边按钮
	 * @param tip
	 * @param icon
	 * @param bgRes TODO
	 */
	public void setLeftButton(String tip,int icon, int bgRes);
	/**
	 * 设置右边按钮
	 * @param tip
	 * @param icon
	 * @param bgRes TODO
	 */
	public void setRightButton(String tip,int icon, int bgRes);
	/**
	 * 设置左边按钮可用
	 * @param isEnabled
	 */
	public void setLeftButtonEnabled(boolean isEnabled);
	/**
	 * 设置右边按钮可用
	 * @param isEnabled
	 */
	public void setRightButtonEnabled(boolean isEnabled);
	/**
	 * 重置TitleBar
	 */
	public void resetTitleBar();
}
