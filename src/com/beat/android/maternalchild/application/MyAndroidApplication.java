package com.beat.android.maternalchild.application;

import com.beat.android.maternalchild.base.BaseApplication;
import com.beat.android.maternalchild.common.Config;
import com.beat.android.maternalchild.utils.LogUtil;

public class MyAndroidApplication extends BaseApplication{

	public static MyAndroidApplication getInstance() {
		return (MyAndroidApplication) BaseApplication.getInstance();
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		LogUtil.e("zlq", "sdcardDir = " + getAppSdcardDir());
	}
	
	@Override
	protected String getAppSdcardDir() {
		return Config.commStoredDiretory;
	}

	@Override
	protected boolean isWriteLogToSdcard() {
		return Config.IS_OUT_LOG;
	}

	@Override
	protected boolean isDebug() {
		return Config.IS_DEBUG;
	}
}
