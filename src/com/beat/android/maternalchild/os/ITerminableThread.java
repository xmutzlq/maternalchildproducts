package com.beat.android.maternalchild.os;

public interface ITerminableThread {
	public void start();
	public void cancel();
	public boolean isCancel();
}
