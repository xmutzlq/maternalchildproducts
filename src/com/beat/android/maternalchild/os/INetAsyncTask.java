package com.beat.android.maternalchild.os;

public interface INetAsyncTask {
	/**
	 * 网络重连后，任务是否需要重新启动
	 * @return
	 */
	public boolean isNeedReStart();
	/**
	 * 异步任务是否停止，用于判断任务是否在执行
	 * @return
	 */
	public boolean isStop();
	/**
	 * 启动任务
	 */
	public void start();
}
