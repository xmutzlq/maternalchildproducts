package com.beat.android.maternalchild.entity;

import java.util.List;

/**
 * 省份数据响应实体
 * com.beat.android.maternalchild.entity.ProvinceResponseEntity
 * @author Martin <br/>
 * create at 2016年1月22日 下午3:07:13
 */
public class ProvinceResponseEntity {
	private int status;

	private String message;

	private String data_version;

	private List<List<ProvinceEntity>> result;

	public void setStatus(int status) {
		this.status = status;
	}

	public int getStatus() {
		return this.status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public void setData_version(String data_version) {
		this.data_version = data_version;
	}

	public String getData_version() {
		return this.data_version;
	}

	public void setResult(List<List<ProvinceEntity>> result) {
		this.result = result;
	}

	public List<List<ProvinceEntity>> getResult() {
		return this.result;
	}
}
