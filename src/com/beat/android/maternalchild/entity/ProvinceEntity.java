package com.beat.android.maternalchild.entity;

/**
 * 省份实体 <br/>
 * com.beat.android.maternalchild.entity.ProvinceEntity
 * @author Martin <br/>
 * create at 2016年1月22日 下午2:49:21
 */
public class ProvinceEntity {
	private int id;
	private String name;
	private String fullname;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	@Override
	public String toString() {
		return "ProvinceEntity [id=" + id + ", name=" + name + ", fullname="
				+ fullname + "]";
	}
}
