package com.beat.android.maternalchild;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beat.android.maternalchild.base.BaseActivity;
import com.beat.android.maternalchild.cache.DataCacheManage;
import com.beat.android.maternalchild.common.Constant;
import com.beat.android.maternalchild.fragment.ClassifyFragment;
import com.beat.android.maternalchild.fragment.HomePageFragment;
import com.beat.android.maternalchild.fragment.UserCenterFragment;
import com.beat.android.maternalchild.utils.CustomToastUtil;
import com.beat.android.maternalchild.widget.TabHostUseInFragment;
import com.beat.android.maternalchild.widget.TabHostUseInFragment.ITabChangeObserver;

public class MainActivity extends BaseActivity implements OnClickListener, ITabChangeObserver{

	private static final int[] TAB_IDS = {Constant.TAB_HOME_PAGE_ID, Constant.TAB_CLASSIFY_ID, Constant.TAB_USER_CENTER_ID};
	private TabHostUseInFragment mTabHost;
	private boolean isActionUpNotify;
	private TextView homePageTV, classifyTV, userCenterTV;
	private ViewGroup scannerBtn, shoppingCarBtn, searchBtn;
	
	private long preTime = -1L;
	public View mHeaderLayout;
	
	@Override
	protected View newContentView(Bundle savedInstanceState) {
		return getLayoutInflater().inflate(R.layout.activity_main_lay, null);
	}
	
	@Override
	protected boolean needLoadView() {
		return false;
	}

	@Override
	protected boolean isTitleBackBtnEnabled() {
		return false;
	}
	
	@Override
	protected boolean headerEnable() {
		return false;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		homePageTV = (TextView) findViewById(R.id.btn_home_page);
		classifyTV = (TextView) findViewById(R.id.btn_classify);
		userCenterTV = (TextView) findViewById(R.id.btn_user_center);
		
		scannerBtn = (FrameLayout) findViewById(R.id.btn_scanner);
		shoppingCarBtn = (FrameLayout) findViewById(R.id.btn_shopping_car);
		searchBtn = (RelativeLayout) findViewById(R.id.rl_input_lay);
		
		mTabHost = (TabHostUseInFragment) findViewById(R.id.tabhost);
		mTabHost.setFragmentParam(this, getSupportFragmentManager());
		mTabHost.setup();
		mTabHost.registTabChangeObserver(this);
		
		initListener();
		initHeaderLayout();
		initTabHost();
		
		homePageTV.setTag(TAB_IDS[0]);
		classifyTV.setTag(TAB_IDS[1]);
		userCenterTV.setTag(TAB_IDS[2]);
	}
	
	private void initHeaderLayout(){
		mHeaderLayout = findViewById(R.id.header_layout);
	}
	
	private void initTabHost() {
		mTabHost.addTab(mTabHost.newTabSpec(TAB_IDS[0] + "").setIndicator(new ViewStub(this)), HomePageFragment.class, null, false);
		mTabHost.addTab(mTabHost.newTabSpec(TAB_IDS[1] + "").setIndicator(new ViewStub(this)), ClassifyFragment.class, null, false);
		mTabHost.addTab(mTabHost.newTabSpec(TAB_IDS[2] + "").setIndicator(new ViewStub(this)), UserCenterFragment.class, null, false);
	}

	private void initListener() {
		homePageTV.setOnClickListener(this);
		classifyTV.setOnClickListener(this);
		userCenterTV.setOnClickListener(this);
		
		scannerBtn.setOnClickListener(this);
		shoppingCarBtn.setOnClickListener(this);
		searchBtn.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View arg0) {
		if(isActionUpNotify) {
			isActionUpNotify = false;
			return;
		}
		int id = arg0.getId();
		switch (id) {
		case R.id.btn_home_page:
			mTabHost.setCurrentTab(TAB_IDS[0]);
			break;
		case R.id.btn_classify:
			mTabHost.setCurrentTab(TAB_IDS[1]);
			break;
		case R.id.btn_user_center:
			mTabHost.setCurrentTab(TAB_IDS[2]);
			break;
		case R.id.btn_scanner:
			//TODO 扫码
			break;
		case R.id.btn_shopping_car:
			//TODO 购物车
			break;
		case R.id.rl_input_lay:
			//TODO 搜索
			break;
		}
	}
	
	@Override
	public void onTabChange(String tabId) {
		int tmpTabId = Integer.parseInt(tabId);
		reSetTabs(tmpTabId);
	}
	
	private void reSetTabs(int tabId) {
		Resources resource = (Resources) getBaseContext().getResources();  
		ColorStateList csl = (ColorStateList) resource.getColorStateList(R.drawable.bg_tab_text_font);
		switch (tabId) {
		case Constant.TAB_HOME_PAGE_ID:
			homePageTV.setTextColor(getResources().getColor(R.color.app_blue_color));
			classifyTV.setTextColor(csl);
			userCenterTV.setTextColor(csl);
			homePageTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.home_selected), null, null);
			classifyTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bg_tab_classify), null, null);
			userCenterTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bg_tab_user_center), null, null);
			break;
		case Constant.TAB_CLASSIFY_ID:
			homePageTV.setTextColor(csl);
			classifyTV.setTextColor(getResources().getColor(R.color.app_blue_color));
			userCenterTV.setTextColor(csl);
			homePageTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bg_tab_home_page), null, null);
			classifyTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.category_selected), null, null);
			userCenterTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bg_tab_user_center), null, null);
			break;
		case Constant.TAB_USER_CENTER_ID:
			homePageTV.setTextColor(csl);
			classifyTV.setTextColor(csl);
			userCenterTV.setTextColor(getResources().getColor(R.color.app_blue_color));
			homePageTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bg_tab_home_page), null, null);
			classifyTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.bg_tab_classify), null, null);
			userCenterTV.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.usercenter_selected), null, null);
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		long currentTime = System.currentTimeMillis();
	    if (currentTime - this.preTime <= 1000L) {
	      DataCacheManage.getInstance().clearAllData();
	      super.onBackPressed();
	    } else {
	    	this.preTime = currentTime;
	    	CustomToastUtil.getInstance(this).showExitToast(getResources().getString(R.string.back_key_finish_app_tip), 2000L);
	    }
	}
	
	@Override
	protected void onDestroy() {
		mTabHost.unregistTabChangeObserver(this);
		CustomToastUtil.getInstance(this).cancelToast();
		dispatchExitApp();
		super.onDestroy();
		android.os.Process.killProcess(android.os.Process.myPid());
	}
}
