package com.beat.android.maternalchild.net.extra;

public final class AreaResultCode {
	public static final int RESULT_SUCCESS = 0; //成功
	public static final int RESULT_PARAM_ERROR = 310; //请求参数信息有误
	public static final int RESULT_KEY_ERROR = 311; //key格式错误
	public static final int RESULT_PROTECTED_ERROR = 306; //请求有护持信息请检查字符串
	public static final int RESULT_UNAUTHORIZE_ERROR = 110; //请求来源未被授权
}
