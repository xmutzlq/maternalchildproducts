package com.beat.android.maternalchild.net;

import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;

public class AbsConnect {
	// request
	public static final String HEADER_REQUEST_CLIENT_AGENT = "Client-Agent";
	// request
	protected static final String HEADER_REQUEST_X_UP_CALLING_LINE_ID = "x-up-calling-line-id";
	public static final String HEADER_REQUEST_PHONE_NUMBER = "phone-number";
	// request
	public static final String HEADER_REQUEST_USER_ID = "token";
	// request
	protected static final String HEADER_REQUEST_API_VERSION = "APIVersion";
	// option
	protected static final String HEADER_REQUEST_CONTENT_TYPE = "Content-Type";
	// option
	protected static final String HEADER_REQUEST_ACCEPT_CHARSET = "Accept-Charset";
	// option
	protected static final String HEADER_REQUEST_COOKIE = "Cookie";
	// request
	protected static final String HEADER_REQUEST_ACTION = "Action";

	protected static final String HEADER_REQUEST_X_ONLINE_HOST = "X-Online-Host";

	protected static final String HEADER_REQUEST_USER_AGENT = "User-Agent";

	protected static final String HEADER_REQUEST_X_REFERRED = "x-referred";

	public static final String HEADER_REQUEST_USER_TYPE = "userType";
	//option
	public static final String HEADER_REQUEST_GUEST_ID = "guest-id";
	//option
	public static final String HEADER_REQUEST_CLIENT_IMSI = "client-imsi";
	//option
	public static final String HEADER_REQUEST_CLIENT_IMEI = "client-imei";
	
	// option
	protected static final String HEADER_RESPONSE_CONTENT_ENCODING = "Content-Encoding";
	// request
	protected static final String HEADER_RESPONSE_RESULT_CODE = "result-code";
	// option
	protected static final String HEADER_RESPONSE_CONTENT_TYPE = "Content-Type";
	// option
	protected static final String HEADER_RESPONSE_SET_COOKIE = "Set-Cookie";
	// request
	protected static final String HEADER_RESPONSE_API_VERSION = "APIVersion";
	// request
	protected static final String HEADER_RESPONSE_TIMESTAMP = "TimeStamp";

	protected static final String HEADER_RESPONSE_X_CLIENT_TYPE = "X-ClientType";

	public static final int CONNET_TYPE_NORMAL = 2;

	protected static final String API_VERSION = "1.0.0";
	protected static final String ACCEPT_CHARSET = "utf-8";
	protected static final String USER_AGENT = "Openwave";
	protected static final String X_REFERRED = "10.118.156.56";
	
	private static final int TIME_OUT_CON = 40 * 1000;
	private static final int TIME_OUT_SO = 35 * 1000;

	public static DefaultHttpClient getDefaultHttpClient(Context context) {
		DefaultHttpClient httpClient = getHttpClient();
		return httpClient;
	}

	public static DefaultHttpClient getHttpClient() {
		// 创建 HttpParams 以用来设置 HTTP 参数（这一部分不是必需的）
		HttpParams params = new BasicHttpParams();
		// 设置连接超时和 Socket 超时，以及 Socket 缓存大小
		HttpConnectionParams.setConnectionTimeout(params, TIME_OUT_CON);
		HttpConnectionParams.setSoTimeout(params, TIME_OUT_SO);
		HttpConnectionParams.setSocketBufferSize(params, 8192);
		// 设置重定向，缺省为 true
		HttpClientParams.setRedirecting(params, true);
		DefaultHttpClient httpClient = new DefaultHttpClient(params);
		return httpClient;
	}
	
	public HttpURLConnection getURLConnection(Context context, URL url) throws Exception{
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		// 在开始和服务器连接之前，可能需要设置一些网络参数
		connection.setConnectTimeout(TIME_OUT_CON);
		connection.setDoOutput(true);// 使用 URL 连接进行输出
		connection.setDoInput(true);// 使用 URL 连接进行输入
		connection.setUseCaches(true);// 忽略缓存
		connection.setRequestMethod("GET");// 设置URL请求方法
		return connection;
	}
}
