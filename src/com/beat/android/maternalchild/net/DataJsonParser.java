package com.beat.android.maternalchild.net;

import android.content.Context;
import android.text.TextUtils;

import com.beat.android.maternalchild.cache.DataCache;
import com.beat.android.maternalchild.common.ClientInfoUtil;
import com.beat.android.maternalchild.net.data.DataConnectInterface;
import com.beat.android.maternalchild.net.data.DataProcess;

public class DataJsonParser extends DataProcess{

	private static DataJsonParser instance;
	
	public static DataJsonParser getInstance(Context context) {
		if (instance == null) {
			instance = new DataJsonParser(context.getApplicationContext());
		}
		return instance;
	}
	
	public DataJsonParser(Context context) {
		super(context);
	}
	
	@Override
	protected DataConnectInterface getHttpConnect(Context context) {
		return new HttpConnect(context);
	}

	@Override
	protected void setUserID(String userId) {
		ResultCodeControl.getServerErrStr(context);
	}

	@Override
	protected void setPhoneNumber(String phoneNumber) {
		if(!TextUtils.isEmpty(phoneNumber)){
			DataCache.getInstance().setPhoneNum(phoneNumber);
		}
	}

	@Override
	protected void setIsNewUser(boolean isNewUser) {
	}

	@Override
	protected String getCurrentClientVersion() {
		return ClientInfoUtil.DEFAULT_CLIENT_VERSION;
	}

	@Override
	protected String getResultString(Context context, String resultCode) {
		return ResultCodeControl.getResultString(context, resultCode);
	}

	@Override
	protected String getServerErrStr(Context context) {
		return ResultCodeControl.getServerErrStr(context);
	}
}
