package com.beat.android.maternalchild.net.data;

/**
 * 请求消息封装
 */
public class RequestData {

	/** 请求方式：POST方式 */
	public static final String REQUEST_METHOD_POST = "POST";
	/** 请求方式：GET方式 */
	public static final String REQUEST_METHOD_GET = "GET";

	// 请求的Action
	public String action;
	// 请求方法
	public String requestMethod;
	// 请求消息体
	public String sendData;
	// 外部动作名称
	public String actionName;
	
	public boolean isUpLoadFile = false;

	public static final int CONTENT_TYPE_BINARY = 1;
	public static final int CONTENT_TYPE_MULTIPART = 2;
	public int contentType;

	public String requestURL;
	
	public RequestData(){
		
	}
	
	public RequestData(String requestMethod, String sendData, String action, String actionName){
		this.requestMethod = requestMethod;
		this.sendData = sendData;
		this.action = action;
		this.actionName = actionName;
	}
	
	public RequestData(String requestURL, String requestMethod, String sendData, String action, String actionName){
		this.requestMethod = requestMethod;
		this.sendData = sendData;
		this.action = action;
		this.actionName = actionName;
		this.requestURL = requestURL;
	}
}
