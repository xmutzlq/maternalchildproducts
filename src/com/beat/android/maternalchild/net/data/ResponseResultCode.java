package com.beat.android.maternalchild.net.data;


/** 响应状态码的定义
 */
public final class ResponseResultCode {

	// 返回状态码
	public static final String STATUS_FAULT = "-1";// 失败

	public static final String STATUS_OK = "0";// 成功
	public static final String STATUS_PROTOCOL_ERROR = "2001";// 协议版本不支持
	public static final String STATUS_USER_ERROR = "2002"; // 非法的用户标识
	public static final String STATUS_INVALID_CLIENT = "2003"; // 非法的客户端
	public static final String STATUS_INVALID_REQUEST = "2004"; // 非法的请求接口
	public static final String STATUS_MISSING_PARAM = "2005"; // 请求参数丢失
	public static final String STATUS_INVALID_PARAM = "2006"; // 无效的参数
	public static final String STATUS_UNSUPPORTED_OPERATED = "2006"; // 不支持的操作
	public static final String STATUS_NOT_LOGIN = "2008"; // 会话超时/未登录
	public static final String STATUS_INVALID_USERINFO = "2009"; // 不支持的用户信息参数
	public static final String STATUS_INVALID_PHONE = "2010"; // 无效的手机号码
	public static final String STATUS_TELNUM_ERROR = "2011"; // 无效的手机号码
	public static final String STATUS_INVALID_CHANNEL = "2012"; // 无效的频道分栏标识
	public static final String STATUS_INVALID_CONTENT = "2013"; // 无效的内容标识
	public static final String STATUS_INVALID_RANK_TYPE = "2014"; // 无效的排行榜类型
	public static final String STATUS_INVALID_TIME_FORMAT = "2015"; // 无效的时间格式
	public static final String STATUS_USER_UNORDER = "2016"; // 用户未订购
	public static final String STATUS_DUPLICATE_RECORDS = "2017"; // 重复的记录
	public static final String STATUS_USER_BOOKMARK_HAD_EXITS = "2018"; // 书签已存在
	public static final String STATUS_EXITS_IN_FAVORITE = "2019"; // 内容已被收藏
	public static final String STATUS_UPDATE_SCHEDULED = "2020"; // 更新通知已预定
	public static final String STATUS_UPDATE_UNSCHEDULED = "2021"; // 更新通知未预定
	public static final String STATUS_LLLEGAL_USER_STATUS = "2022"; // 非法的用户状态
	public static final String STATUS_SUBSCRIBED_FAILED = "2023"; // 业务订购失败
	public static final String STATUS_CANNEL_SUBSCRIBED_FAILED = "2024"; // 业务取消订购失败
	public static final String STATUS_NO_ACCESS_CONTENT = "2025"; // 没有内容的访问权限
	public static final String STATUS_VERIFICATION_CODE_ERR_OR_EXPIRED = "2026"; // 验证码错误或者验证码已过期
	public static final String STATUS_USER_CONTROL_OVER_DAY_LIMIT = "2027"; // 用户操作已达到每日次数限制（推荐/赠送/邀请）
	public static final String STATUS_USER_CONTROL_OVER_LIMIT = "2028"; // 操作超过上限
	public static final String STATUS_ORDER_FAULT = "2029"; // 购买失败
	public static final String STATUS_HAD_VOTED = "2030"; // 用户已投票
	public static final String STATUS_SYSTEM_BOOKMARK_NOT_EXIST = "2031"; // 系统书签不存在
	public static final String STATUS_USER_CAN_NOT_COMMENT = "2032"; // 用户已被禁言
	public static final String STATUS_CONTENT_CAN_NOT_COMMENT = "2033"; // 内容禁止评论
	public static final String STATUS_CLIENT_VERSION_NOT_EXIST = "2034"; // 客户端版本号不存在
	public static final String STATUS_HEADER_INFO_NOT_EXIST = "2035"; // 必要的报头信息不存在
	public static final String STATUS_BODY_INFO_NOT_EXIST = "2036"; // 必要的报文信息不存在
	public static final String STATUS_HAD_COMMENTED = "2037"; // 用户已评论过这边书
	public static final String STATUS_ERR_USERNAME_PSW = "2038"; // 异网用户登陆失败 // 用户名或者密码错误
	public static final String STATUS_VERIFY_SEND_FAILED = "2039"; // 验证码发送失败
	public static final String STATUS_VERIFY_ERROR = "2040"; // 验证码错误
	public static final String STATUS_GET_MDN_FAILED_BY_IMSI = "2041"; // 根据imsi无法从udb获取mdn手机号码
	public static final String STATUS_NOT_ENOUGH_READ_POINT = "2042"; // 阅点不足
	public static final String STATUS_USER_NAME_OR_PSW_INPUT_OVER_LIMIT = "2043"; // 账号密码输入错误次数达到平台设置上限
	public static final String STATUS_EMAIL_EXIST = "2044";	//用户邮箱已存在
	public static final String STATUS_USER_NAME_EXIST = "2045";	//用户名已存在
	public static final String STATUS_USERNAME_PSW_NULL = "2046";	//用户或者密码为空
	
	public static final String STATUS_ACCOUNT_NOT_EXIST = "2050";	//账号不存在
	public static final String STATUS_ERROR_CHANNEL = "2051";	//输入渠道错误
	public static final String STATUS_READERPOINT_CARDNUM_PSW_INCORRECT = "2052";	//阅点券卡号与密码错误
	public static final String STATUS_READERPOINT_RECHARGE_HAS_BEEN = "2053";	//此阅点券已经被充值
	public static final String STATUS_READERPOINT_EXPIRED = "2054";	//此阅点券已经超过充值截止日期
	public static final String STATUS_RECHARGE_FAILURE = "2055";	//充值失败
	
	public static final String STATUS_EXCHANGE_BEYONG_LIMIT = "2101";	//兑换数量超出单次上限
	public static final String STATUS_LACK_OF_USER_POINTS = "2102";	//用户积分不足
	public static final String STATUS_USERID_NOT_EXIST = "2103";	//用户id不存在
	public static final String STATUS_REDEEM_RULE_NOT_EXIST = "2104";	//积分兑换规则不存在
	public static final String STATUS_EXCHANGE_LARGER_REMAIN = "2107"; 	//兑换数量大于剩余可兑换数量
	public static final String STATUS_GET_READERPOINT_TICKETS_FAILED = "2108";	//获取阅点卷失败
	public static final String STATUS_EXCHANGE_EXCEEDS_LIMIT = "2109";	//兑换数量超出上限
	public static final String STATUS_QUERY_REMAIN_POINTS_FAILED = "2110";	//查询剩余阅点卷数量失败
	public static final String STATUS_REMAIN_LESS_THAN_NEED_EXCHANGE = "2111";	//剩余阅点卷可兑换数量小于需要兑换的数量
	public static final String STATUS_ABNORMAL_PRODUCT_INTERFACES = "2112";	//产品接口出现异常
	public static final String STATUS_REMAIN_ZERO = "2113";	//剩余可兑换数量为0
	public static final String STATUS_REDEEM_RULE_ENDED = "2114";	//此积分规则已结束兑换
	public static final String STATUS_REDEEM_RULE_NOT_BEGIN = "2115";	//积分兑换规则还没开始
	public static final String STATUS_HAVE_BEEN_GIFTED = "2116";	//已经赠送
	public static final String STATUS_AUDIO_AUTH_FAILED = "2117";	//有声token鉴权不通过
	public static final String STATUS_GIFT_NON_READER_USER = "2118";	//赠送账号非阅读用户
	public static final String STATUS_HAVE_BEEN_SIGNIN = "2119";	//用户已签到
	public static final String STATUS_IMSI_NOT_REGISTERED = "2120";	//该IMSI未注册
	public static final String STATUS_BOOK_NO_COPYRIGHT = "2221";	//该书籍无版权
	public static final String STATUS_NO_SERIALBOOK = "2222";	//该书籍无版权
	public static final String STATUS_NO_SEQUEL = "2223";	//当前内容无下一期（集）
	public static final String STATUS_NO_NEW_START_IMAGE = "2224";	//无启动界面图片信息
	public static final String STATUS_NO_ADVERTISING_INFO = "2225";	//广告下发关闭
	public static final String STATUS_SMS_ORDER_AUTH_FAILED = "2226";	//短代鉴权失败
	public static final String STATUS_SMS_UNORDER = "2227";	//短代未支付
	public static final String STATUS_OPERATION_EVER_EXECUTE = "2228";	//该操作已执行，当前无需重复执行
	public static final String STATUS_OTHER_CLIENT_REQUEST_ERROR = "2999"; //其他客户端请求错误
	public static final String STATUS_REQUEST_TIMEOUT = "3001"; // 请求超时
	public static final String STATUS_SERVER_BUSY = "3002"; // 服务器忙
	public static final String STATUS_SERVER_DATABASE_ERROR = "3004"; // 服务器数据库异常
	public static final String STATUS_SERVER_UNSUPPORT = "3010"; // 服务器暂时不支持此功能
	public static final String STATUS_SERVER_ERROR_OTHER = "3999"; // 其它服务器错误

}
