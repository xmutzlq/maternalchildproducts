package com.beat.android.maternalchild.net.data;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.beat.android.maternalchild.entity.ProvinceEntity;
import com.beat.android.maternalchild.entity.ProvinceResponseEntity;
import com.beat.android.maternalchild.net.exception.ResultCodeException;
import com.beat.android.maternalchild.net.exception.ServerErrException;
import com.beat.android.maternalchild.net.extra.AreaResultCode;
import com.beat.android.maternalchild.utils.JsonUtils;
import com.beat.android.maternalchild.utils.LogUtil;

import android.content.Context;

/** 
 * 服务器数据接口解析
 * @author zlq
 */
public abstract class DataProcess {

	private static final String TAG = DataProcess.class.getSimpleName();

	private DataConnectInterface connect;
	protected Context context;

	public DataProcess(Context context) {
		connect = getHttpConnect(context);
		this.context = context;
	}
	
	protected abstract DataConnectInterface getHttpConnect(Context context);

	/**
	 * JSon解析
	 * @param content
	 */
	protected void parserJson(String content) { }

	/**
	 * JSon解析
	 * @param response
	 */
	protected void parserJson(ResponseData response) { }

	protected void print(String content) {
		try {
			LogUtil.i(TAG, "result json " + content);
		} catch (Exception e) {
			LogUtil.e(TAG, "print", e);
		}
	}

	/**
	 * 注册接口
	 */
	public boolean register() {
		RequestData requestData = new RequestData();
		requestData.requestMethod = RequestData.REQUEST_METHOD_POST;
		requestData.action = RequestAction.REGISTER;
		requestData.actionName = "注册";
		ResponseData response = connect.connection(requestData);
		if (response != null) {
			
		}
		return false;
	}
	
	/**
	 * 获取省份
	 * @return
	 */
	public List<ProvinceEntity> queryProvinces() throws ServerErrException, ResultCodeException{
		RequestData requestData = new RequestData();
		requestData.requestMethod = RequestData.REQUEST_METHOD_GET;
		requestData.requestURL = "http://apis.map.qq.com/ws/district/v1/getchildren?&id=350000&key=PN3BZ-NY4CD-XH54B-PRCSY-52LL5-3KBRI";
		ResponseData response = connect.connection(requestData);
		if (response != null && response.resultContent != null) {
			try {
				String tempStr = new String(response.resultContent.toByteArray(), "UTF-8");
				ProvinceResponseEntity entity = (ProvinceResponseEntity) JsonUtils.parseJson2Obj(tempStr, ProvinceResponseEntity.class);
				if(entity != null && entity.getStatus() == AreaResultCode.RESULT_SUCCESS){
					return entity.getResult().get(0);
				} else {
					throw newResultCodeException(context, entity.getStatus() + "");
				}
			} catch (UnsupportedEncodingException e) {
				throw newResultCodeException(context, response.resultCode);
			}
		}else {
			throw new ServerErrException("");
		}
	}
	
	public String testDataProcess() throws ServerErrException, ResultCodeException{
		RequestData requestData = new RequestData();
		requestData.requestMethod = RequestData.REQUEST_METHOD_GET;
		requestData.sendData = "/user/detail";
		requestData.action = "";
		requestData.actionName = "测试内容";
		ResponseData response = connect.connection(requestData);
		if (response != null && response.resultContent != null) {
			try {
				String tempStr;
				tempStr = new String(response.resultContent.toByteArray(), "UTF-8");
				LogUtil.v(TAG, "result xml " + tempStr);
				return tempStr;
			} catch (UnsupportedEncodingException e) {
				LogUtil.e(TAG, "order area", e);
				throw newResultCodeException(context, response.resultCode);
			}
		} else {
			throw new ServerErrException("");
		}
	}
	
	protected abstract void setUserID(String userId);
	
	protected abstract void setPhoneNumber(String phoneNumber);
	
	protected abstract void setIsNewUser(boolean isNewUser);
	
	protected abstract String getCurrentClientVersion();
	
	protected ResultCodeException newResultCodeException(Context context, String resultCode) {
		return newResultCodeException(getResultString(context, resultCode), resultCode);
	}
	
	protected ResultCodeException newResultCodeException(String resultString, String resultCode) {
		return new ResultCodeException(resultString, resultCode);
	}

	protected abstract String getResultString(Context context, String resultCode);

	protected abstract String getServerErrStr(Context context);
	
}
