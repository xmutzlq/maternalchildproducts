package com.beat.android.maternalchild.net.data;

import org.apache.http.util.ByteArrayBuffer;

/**
 * 响应的实体
 */
public class ResponseData {

	public String resultCode = "";
	// public String xmlStr = "";
	public ByteArrayBuffer resultContent;

}
