package com.beat.android.maternalchild.net.data;

/** 
 * 服务器网络接口
 */
public interface DataConnectInterface {
	
	public ResponseData connection(RequestData requestData);

}
