package com.beat.android.maternalchild.net.exception;

/**
 * Result异常
 */
public final class ResultCodeException extends Exception {

	private static final long serialVersionUID = 1L;
	private String resultCode;

	public ResultCodeException(String message, String resultCode) {
		super(message);
		this.resultCode = resultCode;
	}

	public String getResultCode() {
		return resultCode;
	}

}
