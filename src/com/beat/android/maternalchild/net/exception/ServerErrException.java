package com.beat.android.maternalchild.net.exception;

/**
 * 服务器异常
 */
public final class ServerErrException extends Exception {

	private static final long serialVersionUID = 1L;

	public ServerErrException(String message) {
		super(message);
	}

}
