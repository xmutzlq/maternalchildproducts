package com.beat.android.maternalchild.net;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import com.beat.android.maternalchild.net.data.DataConnectInterface;
import com.beat.android.maternalchild.net.data.RequestData;
import com.beat.android.maternalchild.net.data.ResponseData;
import com.beat.android.maternalchild.os.AsyncTaskManage;
import com.beat.android.maternalchild.os.AsyncTaskManage.IAsyncTask;
import com.beat.android.maternalchild.utils.FileUtil;
import com.beat.android.maternalchild.utils.LogUtil;

import android.content.Context;
import android.text.TextUtils;

public class HttpConnect extends AbsConnect implements DataConnectInterface{
	private static final String TAG = HttpConnect.class.getSimpleName();
	
	//测试地址
	private static final String FORMAL_URL = "http://122.224.223.141:8080/im";

	private Context context;

	private static final int BUFFER_SIZE = 100 * 1024;

	public HttpConnect(Context context) {
		this.context = context;
	}

	@Override
	public synchronized ResponseData connection(RequestData requestData) {

		if (requestData == null) {
			return null;
		}
		
		String url;
		
		if(!TextUtils.isEmpty(requestData.requestURL)){
			url = requestData.requestURL;
		} else {
			url = FORMAL_URL;
		}
		
		ResponseData responseData = null;
		try {
			responseData = postConnection(requestData,url);
		} catch (Exception e) {
			url = FORMAL_URL;
			try {
				responseData = postConnection(requestData,url);
			} catch (Exception e1) {}
		}
		LogUtil.i("网络请求过程监听", "退出网络请求 id="+requestData.hashCode());
		return responseData;
	}

	private ResponseData postConnection(final RequestData requestData,String url) throws Exception {
		ResponseData responseData = null;
		DefaultHttpClient httpClient = null;
		InputStream is = null;
		LogUtil.i(TAG, "URL " + url);
		LogUtil.i(TAG, "Request action " + requestData.action);
		LogUtil.i(TAG, "send data " + requestData.sendData);
		try {
			httpClient = getDefaultHttpClient(context);

			HttpResponse httpResponse = null;
			HttpUriRequest httpUriRequest = null;
			if (requestData.requestMethod.equals(RequestData.REQUEST_METHOD_POST)) {
				httpUriRequest = postType(requestData,url);
			} else {
				httpUriRequest = getType(requestData,url);
			}
			LogUtil.i(TAG, "connect type: request header start");
			LogUtil.i(TAG, "URL: " + httpUriRequest.getURI());
			Header[] hs = httpUriRequest.getAllHeaders();
			for (Header header : hs) {
				LogUtil.i(TAG, header.getName() + ": " + header.getValue());
			}
			LogUtil.i(TAG, "connect type: request header end");
			final DefaultHttpClient tempHttpClient = httpClient;
			int result = AsyncTaskManage.getInstance().registerHttpTask(new IAsyncTask() {
				@Override
				public void onCancel() {
					LogUtil.i("网络请求过程监听", "执行网络中断代码 id="+requestData.hashCode());
					tempHttpClient.getConnectionManager().shutdown();
				}
			});
			LogUtil.i("网络请求过程监听", "开始请求网络--> 状态="+ (result == AsyncTaskManage.RESULT_STOP ? "线程被中断":"正常执行") +" id="+requestData.hashCode());
			if(result == AsyncTaskManage.RESULT_STOP){
				return null;
			}
			
			httpResponse = httpClient.execute(httpUriRequest);
			int httpCode = httpResponse.getStatusLine().getStatusCode();
			LogUtil.i(TAG, "http status code " + httpCode);
			if (httpCode != HttpStatus.SC_OK) {
				httpUriRequest.abort();
				return null;
			}

			responseData = new ResponseData();

			Header header = httpResponse.getFirstHeader(HEADER_RESPONSE_API_VERSION);
			header = httpResponse.getFirstHeader(HEADER_RESPONSE_RESULT_CODE);
			if (header != null) {
				LogUtil.i(TAG, "header: " + header.getName() + " value " + header.getValue());
				responseData.resultCode = header.getValue();
			}
			Header[] headers = httpResponse.getAllHeaders();
			for (Header head : headers) {
				LogUtil.i(TAG, "header: " + head.getName() + " value "
						+ head.getValue());
			}

			HttpEntity entity = httpResponse.getEntity();
			if (entity == null) {
				return null;
			}
			is = entity.getContent();

			LogUtil.i(TAG, "is lenght " + is.available());
			byte[] responseByteArray = new byte[BUFFER_SIZE];
			ByteArrayBuffer bab = new ByteArrayBuffer(BUFFER_SIZE);
			int line = -1;
			while ((line = is.read(responseByteArray)) != -1) {
				bab.append(responseByteArray, 0, line);
				responseByteArray = new byte[BUFFER_SIZE];
			}
			LogUtil.i(TAG, "bab length " + bab.length());
			responseData.resultContent = bab;

		} catch (Exception e) {
			LogUtil.e("postConnection ", e);
			throw e;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				LogUtil.e("close is", e);
			}
			if (httpClient != null) {
				httpClient.getConnectionManager().shutdown();
				httpClient = null;
			}
		}
		return responseData;
	}

	private HttpPost postType(RequestData requestData,String url) throws UnsupportedEncodingException {
		HttpPost httpPost = null;

		StringBuilder urlSb = new StringBuilder();
		urlSb.append(url);
		String urlStr = urlSb.toString();
		httpPost = new HttpPost(urlStr);
		
		setCommonHeader(httpPost, requestData.action);
		httpPost.setHeader(HEADER_REQUEST_ACTION, requestData.action);

		HttpEntity entity = null;
		if(requestData.isUpLoadFile && FileUtil.isFileExists(requestData.sendData)){
			if (requestData.contentType == RequestData.CONTENT_TYPE_BINARY) {
				File file = new File(requestData.sendData);
				FileInputStream fileInputStream;
				try {
					fileInputStream = new FileInputStream(file);
					InputStreamEntity reqEntity = new InputStreamEntity(fileInputStream, file.length());
					reqEntity.setContentType("binary/octet-stream");
					entity = reqEntity;
				}catch(Exception ignore) {}
			} else if (requestData.contentType == RequestData.CONTENT_TYPE_MULTIPART) {
				httpPost.removeHeaders(HEADER_REQUEST_CONTENT_TYPE);
				File file = new File(requestData.sendData);
	            FileBody fileBody = new FileBody(file);
	            MultipartEntity entitys = new MultipartEntity();
	            entitys.addPart("file", fileBody);
	            entity = entitys;
			}
		}else{
			byte[] sendData = requestData.sendData.getBytes("UTF-8");
			entity = new ByteArrayEntity(sendData);
		}
		httpPost.setEntity(entity);

		return httpPost;
	}

	private HttpGet getType(RequestData requestData,String url) {
		StringBuilder sb = new StringBuilder();
		sb.append(url);
		if (!TextUtils.isEmpty(requestData.sendData)) {
			sb.append(requestData.sendData);
		}
		HttpGet httpGet = getHttpGet(sb.toString(), requestData.action);
//		httpGet.setHeader(HEADER_REQUEST_ACTION, requestData.action);
		sb = null;
		return httpGet;
	}
	
	public static HttpGet getHttpGet(String url) {
		return getHttpGet(url, null);
	}

	private static HttpGet getHttpGet(String url, String action) {
		HttpGet httpGet = null;
		httpGet = new HttpGet(url);
		setCommonHeader(httpGet, action);
		return httpGet;
	}
	
	private static void setCommonHeader(HttpUriRequest httpUriRequest, String action){
		String userID = "";
		if (!TextUtils.isEmpty(userID)) {
			httpUriRequest.setHeader(HEADER_REQUEST_USER_ID, userID);
		} 
//		httpUriRequest.setHeader(HEADER_REQUEST_API_VERSION, API_VERSION);
//		httpUriRequest.setHeader(HEADER_REQUEST_USER_AGENT, USER_AGENT);
//		httpUriRequest.setHeader(HEADER_REQUEST_CONTENT_TYPE, "*/*");
//		httpUriRequest.setHeader(HEADER_REQUEST_X_REFERRED, X_REFERRED);
//		httpUriRequest.setHeader(HEADER_REQUEST_ACCEPT_CHARSET, ACCEPT_CHARSET);
	}
}
