package com.beat.android.maternalchild.utils;

import com.beat.android.maternalchild.common.theme.ThemeController;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PreferencesUtil {
	private static final String PREFS_MODULE_INFO = "info_prefs";
	
	// 主题风格
	private static final String TAG_THEME = "theme";
	
	private static PreferencesUtil instance;
	private Context context;

	private PreferencesUtil(Context context) {
		this.context = context;
	}

	public static PreferencesUtil getInstance(Context context) {
		if (instance == null) {
			instance = new PreferencesUtil(context.getApplicationContext());
		}
		return instance;
	}

	public SharedPreferences getSharedPreferences() {
		return context.getSharedPreferences(PREFS_MODULE_INFO,
				Context.MODE_PRIVATE);
	}

	public Editor getEditor() {
		SharedPreferences pref = getSharedPreferences();
		return pref.edit();
	}
	
	/**
	 * 设置主题
	 * 
	 * @param themeType
	 * @return
	 */
	public boolean setTheme(int themeStyle) {
		Editor edit = getEditor();
		edit.putInt(TAG_THEME, themeStyle);
		return edit.commit();
	}

	/**
	 * 获取主题
	 * 
	 * @return
	 */
	public int getTheme() {
		return getSharedPreferences().getInt(TAG_THEME,
				ThemeController.THEME_STYLE_FASHION);
	}
}
