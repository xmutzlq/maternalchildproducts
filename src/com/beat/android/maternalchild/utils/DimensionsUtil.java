package com.beat.android.maternalchild.utils;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.View.MeasureSpec;

import com.beat.android.maternalchild.base.BaseApplication;

/**
 * 尺寸计算工具类
 */
public class DimensionsUtil {
	private static Context getContext(){
		return BaseApplication.getInstance();
	}
	
	private static Resources getResources(){
		return getContext().getResources();
	}
	
	public static float getDisplayMetric(){
		return getResources().getDisplayMetrics().density;
	}
	
	public static int DIPToPX(float dipValue){ 
        final float scale = getResources().getDisplayMetrics().density; 
        return (int)(dipValue * scale + 0.5f);
	}
	
	public static int PXToDIP(float pxValue){ 
        final float scale = getResources().getDisplayMetrics().density; 
        return (int)( ( pxValue - 0.5f) / scale); 
	}
	
	public static void measureView(View view){
		final int widthMeasureSpec =
			    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		final int heightMeasureSpec =
		    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
		view.measure(widthMeasureSpec, heightMeasureSpec);
	}
	
}
