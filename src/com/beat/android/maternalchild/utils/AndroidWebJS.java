package com.beat.android.maternalchild.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;

public class AndroidWebJS implements IProguardFilter{
	private static final String TAG = AndroidWebJS.class.getSimpleName();
	
	private WebView mWebView;
	private Handler mHandler;
	private Activity mActivity;
	
	public AndroidWebJS(Activity activity,WebView webView){
		mWebView = webView;
		mHandler = new Handler(Looper.getMainLooper());
		mActivity = activity;
	}
}
