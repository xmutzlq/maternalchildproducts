package com.beat.android.maternalchild.utils;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

/**
 * JsonUtils工具类：
 * 
 * @description 采用Gson，google的一个开源项目,实现json对象的解析以及json对象跟 
 *              java对象的互转
 * @author yesb
 */
public class JsonUtils {

	private static final String Tag = "JsonUtils";

	/**
	 * 根据key获取json对象
	 * 
	 * @param jsonData
	 *            json格式字符串
	 * @param key
	 *            json键
	 * @return json值
	 */
	public static String getJsonValue(String jsonData, String key) {
		if (jsonData == null) {
			return null;
		}
		JsonParser parser = new JsonParser();
		JsonObject jsonObject = null;
		try{
			jsonObject = (JsonObject) parser.parse(jsonData);
			if(jsonObject == null) {
				return null;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		if(jsonObject == null){
			return null;
		}
		
		JsonElement jsonElement = jsonObject.get(key);
		if (jsonElement == null) {
			return null;
		}
		
		if(jsonElement.isJsonNull()){
			return null;
		}
		
		return jsonElement.getAsString();
	}

	/**
	 * 将json对象转换成java对象
	 * 
	 * @param <T>
	 *            java类
	 * @param jsonData
	 *            json格式字符串
	 * @param javaClass
	 *            java类
	 * @return java对象
	 */
	public static <T> Object parseJson2Obj(String jsonData, Class<T>

	javaClass) {
		if (jsonData == null) {
			return null;
		}
		Gson gson = new Gson();
		Object obj = gson.fromJson(jsonData, javaClass);
		return obj;
	}

	/**
	 * 将java对象转换成json对象
	 * 
	 * @param obj
	 *            java对象
	 * @return json格式的字符串
	 */
	public static String parseObj2Json(Object obj) {
		if (obj == null) {
			return null;
		}
		try{
			Gson gson = new Gson();
			String objstr = gson.toJson(obj);
			Log.d(Tag, objstr + "上传服务器json");
			return objstr;
		}catch(Exception e){
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * 将json对象转换成java对象集合(此方法还有问题，暂时用parseJson2Object()
	 * 
	 * 方法)
	 * 
	 * @param <T>l java类
	 * @param jsonData
	 *            json格式的字符串
	 * @param javaClass
	 *            java类
	 * @return java对象集合
	 */
	@Deprecated
	public static <T> List<T> parseJson2List(String jsonData, Class<T>

	javaClass) {

		Type listType = new TypeToken<List<T>>() {
		}.getType();
		Gson gson = new Gson();
		List<T> list = gson.fromJson(jsonData, listType);
		return list;
	}

	public static <T> LinkedList<T> parseJson03(String jsonData, Class<T> c)

	{
		Type listType = new TypeToken<LinkedList<T>>() {
		}.getType();
		Gson gson = new Gson();
		LinkedList<T> list = gson.fromJson(jsonData, listType);

		// for (Iterator iterator = users.iterator(); iterator.hasNext();) {
		// User user = (User) iterator.next();
		// System.out.println("name--->" + user.getName());
		// System.out.println("age---->" + user.getAge());
		// }

		return list;

	}

	/**
	 * 将json对象转换成java对象集合
	 * 
	 * @param <T>l java类
	 * @param jsonData
	 *            json格式的字符串
	 * @param type
	 * @return java对象
	 */
	public static Object parseJson2Object(String jsonData, Type type) {
		Gson gson = new Gson();
		return gson.fromJson(jsonData, type);
	}

	/**
	 * 将json对象转换成Object数组
	 * 
	 * @param jsonData
	 *            json格式的字符串
	 * @return Object数组
	 */
	public static Object[] parseJson2IntegerArray(String jsonData)
	{
		List<Integer> i=new ArrayList<Integer>();
		JsonReader reader=new JsonReader(new StringReader(jsonData));
		try {
			reader.beginArray();
			while(reader.hasNext())
			{
				i.add(reader.nextInt());
			}
			reader.endArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return i.toArray();
	}
	/**
	 * 测试
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		String jsonData = "{\"data\":[{\"idName\":\"001\"},{\"id\":\"002\"}]}";
		System.out.println(JsonUtils.getJsonValue(jsonData, "data"));

	}

}
