package com.beat.android.maternalchild.utils;

import java.util.List;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;

public class ActivityTaskUtil {
	public static ActivityManager getActivityManager(Context context){
		return (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	}
	
	public static List<RunningTaskInfo> getRunningTasks(Context context){
		return getRunningTasks(context,10000);
	}
	
	public static List<RunningTaskInfo> getRunningTasks(Context context,int taskSize){
		return getActivityManager(context).getRunningTasks(taskSize);
	}
	
	public static RunningTaskInfo getTopActivityTask(Context context){
		RunningTaskInfo runningTaskInfo = null;
		List<RunningTaskInfo> runningTaskInfos = getRunningTasks(context,1);
		if(runningTaskInfos != null && runningTaskInfos.size() == 1){
			runningTaskInfo = runningTaskInfos.get(0);
		}
		return runningTaskInfo;
	}
	
	public static boolean isPackageTaskStart(Context context){
		return isPackageTaskStart(context, null);
	}
	
	public static boolean isPackageTaskStart(Context context,Class<? extends Activity> filterClass){
		return isPackageTaskStart(context, filterClass,null);
	}
	
	public static boolean isPackageTaskStart(Context context,Class<? extends Activity> filterClass,Class<? extends Activity> selfClass){
		List<RunningTaskInfo> tasks = getRunningTasks(context);
		boolean isStart = false;
		if(tasks != null){
			for(RunningTaskInfo runningTaskInfo : tasks){
				if(filterClass == null
						|| !runningTaskInfo.baseActivity.getClassName().equals(filterClass.getName())){
					if( runningTaskInfo.baseActivity.getPackageName().equals(context.getPackageName())){
						if(selfClass == null){
							isStart = true;
						}else{
							if(runningTaskInfo.baseActivity.getClassName().equals(selfClass.getName())){
								if(runningTaskInfo.numRunning > 1){
									isStart = true;
								}
							}else{
								isStart = true;
							}
						}
					}
				}
			}
		}
		return isStart;
	}
	
	public static ComponentName getTopActivity(Context context,Class<? extends Activity> filterClass){
		List<RunningTaskInfo> tasks = getRunningTasks(context);
		if(tasks != null){
			for(RunningTaskInfo runningTaskInfo : tasks){
				if(filterClass == null
						|| !runningTaskInfo.baseActivity.getClassName().equals(filterClass.getName())){
					if( runningTaskInfo.baseActivity.getPackageName().equals(context.getPackageName())){
						return runningTaskInfo.topActivity;
					}
				}
			}
		}
		return null;
	}
	
	public static boolean isBaseActivity(Context context,Class<? extends Activity> targetClass){
		List<RunningTaskInfo> tasks = getRunningTasks(context);
		if(tasks != null){
			for(RunningTaskInfo runningTaskInfo : tasks){
				if( runningTaskInfo.baseActivity.getPackageName().equals(context.getPackageName()) 
						&& runningTaskInfo.baseActivity.getClassName().equals(targetClass.getName())){
					return true;
				}
			}
		}
		return false;
	}
}
