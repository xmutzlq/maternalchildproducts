package com.beat.android.maternalchild.utils;

import com.beat.android.maternalchild.R;
import com.beat.android.maternalchild.base.BaseApplication;
import com.beat.android.maternalchild.widget.AlertDialog;
import com.beat.android.maternalchild.widget.BaseDialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class DialogUtil {
	public static void showAlertDialog(Activity context, String title, String content, 
			OnClickListener okClickListener){
		showAlertDialog(context, title, content, R.string.btn_text_confirm, okClickListener, R.string.btn_text_cancel, null);
	}
	
	public static void showAlertDialog(Activity context, String title, String content, 
			OnClickListener okClickListener, OnClickListener cancelClickListener){
		showAlertDialog(context, title, content, R.string.btn_text_confirm, okClickListener, R.string.btn_text_cancel, cancelClickListener);
	}
	
	public static void showAlertDialog(Activity context, String title, String content, 
			int okResId, OnClickListener okClickListener, int cancelResId, OnClickListener cancelClickListener){
		AlertDialog dialog = new AlertDialog(context);
		dialog.setTitle(title);
		dialog.setContent(content);
		dialog.setButton(okResId, okClickListener, cancelResId, cancelClickListener);
		if (!context.isFinishing()) {
			dialog.show();
		}
	}
	
	public static void showAlertDialog(Activity context, String title, View contentLayout, 
			OnClickListener okClickListener){
		showAlertDialog(context, title, contentLayout, R.string.btn_text_confirm, okClickListener, R.string.btn_text_cancel, null);
	}
	
	public static void showAlertDialog(Activity context, String title, View contentLayout, 
			OnClickListener okClickListener, OnClickListener cancelClickListener){
		showAlertDialog(context, title, contentLayout, R.string.btn_text_confirm, okClickListener, R.string.btn_text_cancel, cancelClickListener);
	}
	
	public static void showAlertDialog(Activity context, String title, View contentLayout, 
			int okResId, OnClickListener okClickListener, int cancelResId, OnClickListener cancelClickListener){
		AlertDialog dialog = new AlertDialog(context);
		dialog.setTitle(title);
		dialog.setContentLayout(contentLayout);
		dialog.setButton(okResId, okClickListener, cancelResId, cancelClickListener);
		if (!context.isFinishing()) {
			dialog.show();
		}
	}
	
	public static Dialog getAlertDialog(Activity context, String title, View contentLayout, 
			int okResId, OnClickListener okClickListener, int cancelResId, OnClickListener cancelClickListener){
		AlertDialog dialog = new AlertDialog(context);
		dialog.setTitle(title);
		dialog.setContentLayout(contentLayout);
		dialog.setButton(okResId, okClickListener, cancelResId, cancelClickListener);
		
		return dialog;
	}
	
	public static void showAlertDialog(Activity context, String title, View contentLayout, FrameLayout.LayoutParams lp, 
			int okResId, OnClickListener okClickListener, int cancelResId, OnClickListener cancelClickListener){
		AlertDialog dialog = new AlertDialog(context);
		dialog.setTitle(title);
		dialog.setContentLayout(contentLayout);
		dialog.setButton(okResId, okClickListener, cancelResId, cancelClickListener);
		if (!context.isFinishing()) {
			dialog.show();
		}
	}
	
	public static Dialog getWaittingDialog(Context context) {
		return getWaittingDialog(context,null);
	}
	
	public static Dialog getWaittingDialog(Context context, int resId) {
		String content = null;
		if (resId > -1) {
			content = context.getString(resId);
		}
		return getWaittingDialog(context, content);
	}
	
	public static Dialog getWaittingDialog(Context context, String content) {
		final BaseDialog dialog = customDialog(context, R.style.WaitingDialog);
		dialog.setGravity(Gravity.CENTER_VERTICAL);
		dialog.setContentView(R.layout.dialog_waitting);
		if (!TextUtils.isEmpty(content)) {
			TextView tv = (TextView) dialog.findViewById(R.id.content_tv);
			tv.setText(content);
		}
		dialog.findViewById(R.id.dialog_cancel).setOnClickListener(
				new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
		return dialog;
	}
	
	public static final BaseDialog customDialog(Context context, int theme) {
		BaseDialog dialog = new BaseDialog(context, theme);
		dialog.setCanceledOnTouchOutside(false);
		return dialog;
	}
	
	public static final void dealDialogBtnWithPrimarySecondary(Button lButton, int lResId, OnClickListener lOnClickListener, 
			Button rButton, int rResId, OnClickListener rOnClickListener){
		Resources resource = BaseApplication.getInstance().getResources();
		if(Build.VERSION.SDK_INT >= 14){//4.0
			int L_Left = lButton.getPaddingLeft();	
			int L_Top = lButton.getPaddingTop();
			int L_Right = lButton.getPaddingRight();
			int L_Bottom = lButton.getPaddingBottom();
			int R_Left = rButton.getPaddingLeft();	
			int R_Top = rButton.getPaddingTop();
			int R_Right = rButton.getPaddingRight();
			int R_Bottom = rButton.getPaddingBottom();
			lButton.setText(rResId);
			lButton.setTextColor(resource.getColor(R.color.btn_blue_content));
			lButton.setBackgroundResource(R.drawable.bg_light_btn);
			lButton.setPadding(L_Left, L_Top, L_Right, L_Bottom);
			lButton.setOnClickListener(rOnClickListener);
			rButton.setText(lResId);
			rButton.setOnClickListener(lOnClickListener);
			rButton.setTextColor(resource.getColor(R.color.white));
			rButton.setBackgroundResource(R.drawable.bg_normal_btn);
			rButton.setPadding(R_Left, R_Top, R_Right, R_Bottom);
		}else{//4.0以下
			lButton.setText(lResId);
			lButton.setOnClickListener(lOnClickListener);
			rButton.setText(rResId);
			rButton.setOnClickListener(rOnClickListener);
		}
	}
	
	public static final void dealDialogBtn(Button lButton, int lResId, OnClickListener lOnClickListener, 
			Button rButton, int rResId, OnClickListener rOnClickListener){
		if(Build.VERSION.SDK_INT >= 14){//4.0
			lButton.setText(rResId);
			lButton.setOnClickListener(rOnClickListener);
			rButton.setText(lResId);
			rButton.setOnClickListener(lOnClickListener);
		}else{//4.0以下
			lButton.setText(lResId);
			lButton.setOnClickListener(lOnClickListener);
			rButton.setText(rResId);
			rButton.setOnClickListener(rOnClickListener);
		}
	}
	
	public static final void dealDialogBtn(Button lButton, int lResId, OnClickListener lOnClickListener, 
			Button rButton, int rResId, OnClickListener rOnClickListener,boolean whichSetEnable){
		dealDialogBtnWithPrimarySecondary(lButton ,lResId ,lOnClickListener ,rButton , rResId , rOnClickListener);
		if(Build.VERSION.SDK_INT >= 14 && whichSetEnable){
			rButton.setEnabled(false);
		}else{
			lButton.setEnabled(false);
		}
	}
}
