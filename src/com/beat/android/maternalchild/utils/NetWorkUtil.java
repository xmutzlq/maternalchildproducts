package com.beat.android.maternalchild.utils;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

public class NetWorkUtil {
	public static final byte CURRENT_NETWORK_TYPE_NONE = 0;
	/** 根据APN区分网络类型 */
	public static final byte CURRENT_NETWORK_TYPE_WIFI = 1;// wifi
	public static final byte CURRENT_NETWORK_TYPE_CTNET = 2;// ctnet
	public static final byte CURRENT_NETWORK_TYPE_CTWAP = 3;// ctwap
	public static final byte CURRENT_NETWORK_TYPE_CMWAP = 4;// cmwap
	public static final byte CURRENT_NETWORK_TYPE_UNIWAP = 5;// uniwap,3gwap
	public static final byte CURRENT_NETWORK_TYPE_CMNET = 6;// cmnet
	public static final byte CURRENT_NETWORK_TYPE_UNIET = 7;// uninet,3gnet

	//WIFI
	public static String APN_WIFI = "WIFI";
	//CDMA
	public static String APN_CDMA = "CDMA";
	public static String APN_CTWAP = "ctwap";
	public static String APN_CTNET = "ctnet";
	//GSM
	public static String APN_CMWAP = "cmwap";
	public static String APN_CMNET = "CMNET";
	//WCDMA
	public static String APN_UNIWAP = "uniwap";
	public static String APN_UNINET = "UNINET";
	public static String APN_3GWAP = "3GWAP";
	public static String APN_3GNET = "3GNET";

	private static final Uri PREFERRED_APN_URI = Uri.parse("content://telephony/carriers/preferapn");
	private static final Uri PREFERRED_35_APN_URI = Uri.parse("content://telephony/carriers-preferapn-cdma");
	private static final Uri CURRENT_APNS = Uri.parse("content://telephony/carriers/current");
	private static final Uri PREFERRED2_APN_URI = Uri.parse("content://telephony/carriers/preferapn2");

	public static boolean isCtwap(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null) {
			String str = networkInfo.toString();
			if (!TextUtils.isEmpty(str) && str.toLowerCase().indexOf(APN_CTWAP) > -1) {
				return true;
			}
		}
		return APN_CTWAP.equalsIgnoreCase(getApnType(context));
	}

	public static boolean isCmwap(Context context) {
		return judgeNetworkType(context, APN_CMWAP);
	}
	
	public static boolean isCmnet(Context context) {
		return judgeNetworkType(context, APN_CMNET);
	}

	public static boolean isUniwap(Context context) {
		return judgeNetworkType(context, APN_UNIWAP);
	}
	
	public static boolean isUninet(Context context) {
		return judgeNetworkType(context, APN_UNINET);
	}

	private static boolean judgeNetworkType(Context context, String type) {
		NetworkInfo info = getActiveNetworkInfo(context);
		if (info != null) {
			String extraInfo = info.getExtraInfo();
			if (!TextUtils.isEmpty(extraInfo) && extraInfo.equalsIgnoreCase(type)) {
				return true;
			}
		}
		return false;
	}

	private static Uri getApnUri(Context context) {
		return PREFERRED_APN_URI;
	}
	
	public static String getCurrentNetType(Context context) {
		NetworkInfo networkInfo = getActiveNetworkInfo(context);
		String type = "nomatch";
		if (networkInfo != null) {
			String typeName = networkInfo.toString();
			if (!TextUtils.isEmpty(typeName)) {
				String temp = typeName.toLowerCase();
				if (temp.indexOf(APN_CTNET) > -1) {// ctnet
					type = APN_CTNET;
				} else if (temp.indexOf(APN_CTWAP) > -1) {// ctwap
					type = APN_CTWAP;
				}
			}
		}

		return type;
	}

	public static String getApnType(Context context) {
		if(Build.VERSION.SDK_INT >= 17){
			return getCurrentNetType(context);
		}
		String apntype = "nomatch";
		Uri uri = getApnUri(context);
		Cursor c = context.getContentResolver().query(uri, null, null, null, null);
		if (c != null) {
			if (c.moveToFirst()) {
				String user = c.getString(c.getColumnIndex("user"));
				if (!TextUtils.isEmpty(user)) {
					if (user.startsWith(APN_CTNET)) {
						apntype = APN_CTNET;
					} else if (user.startsWith(APN_CTWAP)) {
						apntype = APN_CTWAP;
					}
				}
			}
			c.close();
			c = null;
		}
		return apntype;
	}
	
	public static boolean isDifferentNetwork(Context context){
		if(isWifiWork(context)){
			return false;
		}
		NetworkInfo networkInfo = getActiveNetworkInfo(context);
		String typeInfo = "nomatch";
		if (networkInfo != null) {
			typeInfo = networkInfo.toString().toLowerCase();
		}
		if(typeInfo.indexOf("cmnet") != -1
				|| typeInfo.indexOf("cmwap") != -1
				|| typeInfo.indexOf("3gnet") != -1
				|| typeInfo.indexOf("3gwap") != -1
				|| typeInfo.indexOf("uninet") != -1
				|| typeInfo.indexOf("uniwap") != -1){
			return true;
		}
		return false;
	}
	
	public static String getActiveApnType(Context context) {
		
		String apntype = "nomatch";
		if (isWifiWork(context)) {
			apntype = APN_WIFI;
		} else if (isCDMA(context)) {
			if (isCtwap(context)) {
				apntype = APN_CTWAP;
			} else {
				apntype = APN_CTNET;
			}
		} else if (isCmwap(context)) {
			apntype = APN_CMWAP;
		} else if (isCmnet(context)) {
			apntype = APN_CMNET;
		} else if (isUninet(context)) {
			apntype = APN_UNINET;
		} else if (isUniwap(context)) {
			apntype = APN_UNIWAP;
		}
		LogUtil.i("Active Apn Type: " + apntype);
		
		return apntype;
	}

	public static String getCurrentApnType(Context context) {

		LogUtil.v("ApnUtil", "getCurrentApnType");

		String apntype = "nomatch";
		Cursor c = context.getContentResolver().query(CURRENT_APNS, null, null, null, null);
		if (c != null) {
			if (c.moveToFirst()) {
				String user = c.getString(c.getColumnIndex("user"));
				if (user.startsWith(APN_CTNET)) {
					apntype = APN_CTNET;
				} else if (user.startsWith(APN_CTWAP)) {
					apntype = APN_CTWAP;
				}
			}
			c.close();
			c = null;
		}

		return apntype;
	}

	public static String getCurrentCdmaName(Context context) {
		return getCurrentApnName(context, getApnUri(context));
	}

	public static String getCurrentGsmName(Context context) {
		return getCurrentApnName(context, PREFERRED2_APN_URI);
	}

	public static String getCurrentApnName(Context context, Uri uri) {
		String apnName = null;
		Cursor c = context.getContentResolver().query(uri, null, null, null,
				null);
		if (c != null) {
			if (c.moveToFirst()) {
				apnName = c.getString(c.getColumnIndex("name"));
			}
			c.close();
			c = null;
		}
		return apnName;
	}

	/**
	 * 添加新的CTWAP的APN
	 * 
	 * @param context
	 * @return
	 */
	public static long newCtwapApn(Context context) {
		Uri uri = Uri.parse("content://telephony/carriers");
		ContentValues values = new ContentValues();
		values.put("name", "ctwap");
		values.put("apn", "#777");
		values.put("type", "default,mms");
		values.put("user", "ctwap@mycdma.cn");
		values.put("numeric", "46003");
		values.put("mcc", "460");
		values.put("mnc", "03");
		values.put("port", "80");
		values.put("mmsproxy", "10.0.0.200");
		values.put("mmsport", "80");
		values.put("mmsc", "http://mmsc.vnet.mobi");
		values.put("authtype", "-1");
		values.put("password", "vnet.mobi");
		values.put("proxy", "10.0.0.200");
		Uri insertUri = context.getContentResolver().insert(uri, values);
		if (insertUri == null) {
			return -1;
		}
		return ContentUris.parseId(insertUri);
	}

	/**
	 * 获取CTWAP的APN的ID
	 * 
	 * @param context
	 * @return
	 */
	public static long getCtwapAPN(Context context) {

		long id = -1;
		LogUtil.v("ApnUtil", "getCtwapAPN 11");
		Uri uri = Uri.parse("content://telephony/carriers");//获取所有apn
		
		Cursor cr = context.getContentResolver().query(uri, null, null, null, null);

		while (cr != null && cr.moveToNext()) {
			String user = cr.getString(cr.getColumnIndex("user"));
			String numeric = cr.getString(cr.getColumnIndex("numeric"));
			String mmsproxy = null;
			mmsproxy = cr.getString(cr.getColumnIndex("proxy"));
			LogUtil.i("APN Count: " + cr.getCount());
			int columneCount = cr.getColumnCount();
			LogUtil.i("count " + columneCount);
			for(int i = 0; i < columneCount; ++i){
				LogUtil.i(cr.getColumnName(i) + " : " +  cr.getString(i));
			}
			if (TextUtils.isEmpty(user) || TextUtils.isEmpty(numeric) || TextUtils.isEmpty(mmsproxy)) {
				continue;
			}
			if (user.startsWith(APN_CTWAP) && numeric.equals("46003") && mmsproxy.equals("10.0.0.200")) {
				id = cr.getLong(cr.getColumnIndex("_id"));
				break;
			}
		}
		if (cr != null) {
			cr.close();
		}

		if (id == -1) {
			id = newCtwapApn(context);
		}

		return id;
	}

	public static long getCtnetAPN(Context context) {

		long id = -1;
		Uri uri = Uri.parse("content://telephony/carriers");
		Cursor cr = context.getContentResolver().query(uri, null, null, null,
				null);
		while (cr != null && cr.moveToNext()) {
			String user = cr.getString(cr.getColumnIndex("user"));
			String numeric = cr.getString(cr.getColumnIndex("numeric"));
			if (TextUtils.isEmpty(user) || TextUtils.isEmpty(numeric)) {
				continue;
			}
			if (user.startsWith(APN_CTNET) && numeric.equals("46003")) {
				id = cr.getLong(cr.getColumnIndex("_id"));
				break;
			}
		}
		if (cr != null) {
			cr.close();
		}

		return id;
	}

	/**
	 * 设置当前链接方式为CTWAP
	 * 
	 * @param context
	 * @return
	 */
	public static boolean setCtwapMode(Context context) {
		long id = getCtwapAPN(context);
		LogUtil.v("ApnUtil", "setCtwapMode id " + id);
		boolean isSuccess = setCDMAMode(context, id);
		return isSuccess;
	}

	public static boolean setCDMAMode(Context context, long id) {
		if (id == -1) {
			return false;
		}
		ContentValues cv = new ContentValues();
		cv.put("apn_id", id);

		int count = context.getContentResolver().update(getApnUri(context), cv,
				null, null);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 判断是否启动WIFI
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isWifiWork(Context context) {
		WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		if (wm == null) {// 有些手机阉割到WIFI，这里会为null。如：宇龙E230A
			return false;
		}
		if (wm.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			if(cm != null){
				NetworkInfo networkInfo = cm.getActiveNetworkInfo();
				if (networkInfo != null) {
					String ei = networkInfo.getTypeName();
					if (!TextUtils.isEmpty(ei)) {
						int index = ei.toUpperCase().indexOf("WIFI");
						if (index > -1) {
							return true;
						}
					}
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * 关闭WIFI连接
	 * 
	 * @param context
	 * @return
	 */
	public static boolean closeWifi(Context context) {
		WifiManager wm = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		if (wm.isWifiEnabled()) {
			return wm.setWifiEnabled(false);
		} else {
			return true;
		}
	}

	/**
	 * 判断是否有网络连接
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isNetAvailable(final Context context) {
        return isConnected(context);
	}

	public static boolean isCDMA(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null) {
			String ei = networkInfo.getSubtypeName();
			if (!TextUtils.isEmpty(ei)) {
				int index = ei.toUpperCase().indexOf(APN_CDMA);
				if (index > -1) {
					return true;
				}
				ei = networkInfo.getExtraInfo();
				if (!TextUtils.isEmpty(ei)) {
					if (ei.equalsIgnoreCase(APN_CTWAP) || ei.equalsIgnoreCase(APN_CTNET)) {
						return true;
					}
				}
			}
			return false;
		}
		return false;
	}

	/**
	 * 判断网络是否连接上
	 * 
	 * @param context
	 * @return
	 */
	public static boolean isConnected(Context context) {
		NetworkInfo networkInfo = getActiveNetworkInfo(context);
		if (networkInfo != null) {
			if (networkInfo.getState().compareTo(NetworkInfo.State.CONNECTED) == 0) {
				return true;
			}
		}
		return false;
	}

	private static NetworkInfo getActiveNetworkInfo(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		return networkInfo;
	}

	/**
	 * 关闭网络
	 * 
	 * @param context
	 */
	public static void closeNetwork(Context context) {
		// TODO
	}

	public static String getIMSI(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getSubscriberId();
	}
}
