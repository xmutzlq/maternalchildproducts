package com.beat.android.maternalchild.adapter;

import java.util.ArrayList;

public interface ListSelecter
{
	public void setSelectEnabled(boolean isEnabled);
	public boolean isSelectEnabled();
	public ArrayList<?> getSelect();
	public void setSelectData(ArrayList<?> selects);
	public void selectAll();
	public void selectItem(int position);
}
