package com.beat.android.maternalchild.adapter;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;

/**
 * 多选列表
 * 
 * @author zenglq
 * 
 * @param <T>
 */
public abstract class BaseContentSelecterAdapter<T> extends BaseAdapter implements ListSelecter
{
	private Context mContext;
	private ArrayList<T> mDatas;
	private LayoutInflater mInflater;
	private DataSetChangedListener mDataSetChangedListener;
	private boolean isSelect = false;
	private ArrayList<T> mSelectDatas;
	private SelectListener mSelectListener;
	private SelectItemhandler mSelectItemhandler;
	private boolean isSelectAll = false;

	public BaseContentSelecterAdapter(Context context)
	{
		inIt(context, null);
	}

	public BaseContentSelecterAdapter(Context context, ArrayList<T> datas)
	{
		inIt(context, datas);
	}

	private void inIt(Context context, ArrayList<T> datas)
	{
		mContext = context;
		mDatas = datas;
		if (mDatas == null)
		{
			mDatas = new ArrayList<T>();
		}
		mSelectDatas = new ArrayList<T>();
		mInflater = LayoutInflater.from(context);
	}

	public LayoutInflater getLayoutInflater()
	{
		return mInflater;
	}

	public Context getContext()
	{
		return mContext;
	}

	public void setData(ArrayList<T> datas)
	{
		mDatas = datas;
		if (mDatas == null)
		{
			mDatas = new ArrayList<T>();
		}
	}

	public void addData(ArrayList<T> datas)
	{
		mDatas.addAll(datas);
	}

	public void addData(T datas)
	{
		mDatas.add(datas);
	}

	public ArrayList<T> getData()
	{
		return mDatas;
	}

	public void deleteData(ArrayList<T> datas)
	{
		mDatas.removeAll(datas);
	}

	public void deleteData(T datas)
	{
		mDatas.remove(datas);
	}

	public void clearData(ArrayList<T> datas)
	{
		mDatas.clear();
	}

	@Override
	public int getCount()
	{
		return mDatas.size();
	}

	@Override
	public T getItem(int position)
	{
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public void setSelectEnabled(boolean isEnabled)
	{
		if (isSelect != isEnabled)
		{
			isSelect = isEnabled;
			isSelectAll = false;
			mSelectDatas.clear();
			notifyDataSetChanged();
		}
	}

	@Override
	public boolean isSelectEnabled()
	{
		return isSelect;
	}

	@Override
	public void setSelectData(ArrayList<?> selects)
	{
		mSelectDatas.clear();
		if (selects != null && selects.size() > 0)
		{
			mSelectDatas.addAll((ArrayList<T>) selects);
		} else
		{
			notifyDataSetChanged();
		}
	}

	@Override
	public ArrayList<T> getSelect()
	{
		ArrayList<T> temp = new ArrayList<T>();
		temp.addAll(mSelectDatas);
		return temp;
	}

	@Override
	public void selectAll()
	{
		isSelectAll = true;
		notifyDataSetChanged();
	}

	@Override
	public void selectItem(int position)
	{
		T ItemData = getItem(position);
		if (isSelect && ItemData != null)
		{
			if (!mSelectDatas.contains(ItemData))
			{
				mSelectDatas.add(ItemData);
				notifyDataSetChanged();
			}
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			convertView = newConvertView(position, parent);
			convertView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					int position = v.getId();
					if (performSelect(position))
					{
						return;
					}
					if (!getSelectItemEnabled(position))
					{
						return;
					}
					T ItemData = getItem(position);
					if (isSelect && ItemData != null)
					{
						if (mSelectDatas.contains(ItemData))
						{
							mSelectDatas.remove(ItemData);
							setSelectItemCheck(v, false);
							isSelectAll = false;
						} else
						{
							mSelectDatas.add(ItemData);
							setSelectItemCheck(v, true);
						}
					}
				}
			});
		}
		convertView.setId(position);
		fillViewData(position, convertView, isSelect);
		setSelectItemCheckView(convertView, isSelect);
		convertView.setClickable(isSelect);
		if (isSelect)
		{
			boolean isSelectItemEnabled = getSelectItemEnabled(position);
			setSelectItemEnabled(convertView, isSelectItemEnabled);
			if (isSelectItemEnabled)
			{
				T ItemData = getItem(position);
				setSelectItemCheck(convertView, mSelectDatas.contains(ItemData));
			}
		}

		return convertView;
	}

	protected abstract View newConvertView(int position, ViewGroup parent);

	protected abstract void fillViewData(int position, View convertView, boolean isSelect);

	/**
	 * 根据选中状态设置ItemView界面样式
	 * 
	 * @param convertView
	 * @param isSelect
	 */
	protected abstract void setSelectItemCheck(View convertView, boolean isCheck);

	/**
	 * 根据是否启动选择功能，设置ItemView界面样式
	 * 
	 * @param convertView
	 * @param isSelect
	 */
	protected abstract void setSelectItemCheckView(View convertView, boolean isSelect);

	/**
	 * 根据ItemView的选中功能是否可用设置ItemView界面样式
	 * 
	 * @param convertView
	 * @param isEnabled
	 */
	protected abstract void setSelectItemEnabled(View convertView, boolean isEnabled);

	/**
	 * 设置ItemView选中功能是否可用
	 * 
	 * @param position
	 * @return
	 */
	protected boolean getSelectItemEnabled(int position)
	{
		return true;
	};

	public int handleSelectItem(final ArrayList<T> curSelect)
	{
		if (mSelectItemhandler != null)
		{
			return mSelectItemhandler.handleSelectItem(curSelect);
		}
		return 0;
	};

	@Override
	public void notifyDataSetChanged()
	{
		if (isSelectAll)
		{
			mSelectDatas.clear();
			mSelectDatas.addAll(mDatas);
		}

		if (mSelectItemhandler != null)
		{
			mSelectItemhandler.handleSelectItem(mSelectDatas);
		}

		if (mDataSetChangedListener != null)
		{
			mDataSetChangedListener.onDataSetChanged();
		}
		super.notifyDataSetChanged();
	}

	public void setDataSetChangedListener(DataSetChangedListener dataSetChangedListener)
	{
		mDataSetChangedListener = dataSetChangedListener;
	}

	public interface DataSetChangedListener
	{
		public void onDataSetChanged();
	}

	/**
	 * 
	 * @param position
	 * @return 返回true标识事件被消耗
	 */
	private boolean performSelect(int position)
	{
		if (mSelectListener == null)
			return false;
		return mSelectListener.onSelect(position);
	}

	public void setSelectListener(SelectListener selectListener)
	{
		mSelectListener = selectListener;
	}

	public void setSelectItemhandler(SelectItemhandler selectItemhandler)
	{
		mSelectItemhandler = selectItemhandler;
	}

	public interface SelectListener
	{
		public boolean onSelect(int position);
	}

	public interface SelectItemhandler
	{
		public int handleSelectItem(final ArrayList<?> curSelect);
	}

	/**
	 * 排除所选
	 * 
	 * @param position
	 */
	public void rejectSelection(int position)
	{

		if (position >= 0 && position < mSelectDatas.size())
		{

			mSelectDatas.remove(position);

		}

	}

	/**
	 * 排除所选
	 * 
	 * @param position
	 */
	public void rejectSelection(T object)
	{

		if (mSelectDatas.contains(object))
		{
			mSelectDatas.remove(object);
		}
	}
}
