package com.beat.android.maternalchild.common;

import java.io.File;

import com.beat.android.maternalchild.utils.FileUtil;

public class Config {
	public static final String commStoredDiretory = FileUtil.getExternalStorageDirectory()
			+ File.separator + "maternalchild" + File.separator;
	
	/** 存放LOG */
	public static final String FILE_LOG = commStoredDiretory + "log.txt";
	
	// 输出LOG到文件
	public static final boolean IS_OUT_LOG = false;
	
	//Debug模式
	public static final boolean IS_DEBUG = true;
}
