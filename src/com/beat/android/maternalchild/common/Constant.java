package com.beat.android.maternalchild.common;

public final class Constant {
	public static final int TAB_HOME_PAGE_ID = 0; //首页
	public static final int TAB_CLASSIFY_ID = 1; //分类
	public static final int TAB_USER_CENTER_ID = 2; //中心
}
