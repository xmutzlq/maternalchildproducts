package com.beat.android.maternalchild.common.theme;

public abstract class Theme {

	/** 窗口背景图 */
	public abstract int windowBackgroud();

	/** HEADER BAR的背景图 */
	public abstract int headerBarBackgroud();

	public abstract int headerBarColor();

	/** footer bar 的背景 */
	public abstract int footerBarBackgroud();

	public abstract int headerImg();

	public abstract int btnSystemMenu();

	public abstract int btnSystemMenuPressed();

	public abstract int btnFooterBarBtnBgPressed();

	public abstract int textColorChangeChannelTip();

	public abstract int textColorContent();

	public abstract int textChangeChaneleTip();

	public abstract int textColorUserInfoTitle();

	public abstract int textColorSearchTip();

	public abstract int dialogStyle();

	public abstract int toastBackgroud();
}
