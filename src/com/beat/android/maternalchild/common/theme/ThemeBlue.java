package com.beat.android.maternalchild.common.theme;

import com.beat.android.maternalchild.R;

import android.graphics.Color;

public class ThemeBlue extends Theme {

	@Override
	public int btnFooterBarBtnBgPressed() {
		return R.drawable.nav_btn_pressed;
	}

	@Override
	public int footerBarBackgroud() {
		return R.drawable.foot_bar_bg;
	}

	@Override
	public int headerBarBackgroud() {
		return R.drawable.head_bar_bg;
	}

	@Override
	public int windowBackgroud() {
		return R.drawable.window_bg;
	}

	@Override
	public int btnSystemMenu() {
		return R.drawable.btn_system_menu;
	}

	@Override
	public int btnSystemMenuPressed() {
		return R.drawable.btn_system_menu_pressed;
	}

	@Override
	public int headerBarColor() {
		return Color.WHITE;
	}

	@Override
	public int textColorChangeChannelTip() {
		return R.color.yellow;
	}

	@Override
	public int textColorContent() {
		return R.color.black;
	}

	@Override
	public int textColorUserInfoTitle() {
		return R.color.yellow;
	}

	@Override
	public int textColorSearchTip() {
		return R.color.white;
	}

	@Override
	public int dialogStyle() {
		return R.style.CustomDialog;
	}

	@Override
	public int textChangeChaneleTip() {
		return R.string.tip_change_channel;
	}

	@Override
	public int headerImg() {
		return R.drawable.header_img;
	}

	@Override
	public int toastBackgroud() {
		return R.drawable.dialog_bg;
	}
}
