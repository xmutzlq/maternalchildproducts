package com.beat.android.maternalchild.common.theme;

public final class ThemeController {
	
	public static final int THEME_STYLE_FASHION = 1;
	public static final int THEME_STYLE_GOLD = 2;
	public static final int THEME_STYLE_BLUE = 3;
	
	private static Theme theme;
	private static int currentThemeStyle;
	
	private static ThemeController instance;
	
	private ThemeController(){
		currentThemeStyle = 0;
	}
	
	public static ThemeController getInstance(){
		if(instance == null){
			instance = new ThemeController();
		}
		return instance;
	}
	
	public boolean setTheme(int themeIndex){
		if(themeIndex != currentThemeStyle){
			theme = null;
			currentThemeStyle = themeIndex;
			switch (currentThemeStyle) {
			case THEME_STYLE_FASHION:
				theme = new ThemeFashion();
				break;
			case THEME_STYLE_GOLD:
				theme = new ThemeGold();
				break;
			case THEME_STYLE_BLUE:
				theme = new ThemeBlue();
				break;
			default:
				theme = new ThemeFashion();
				break;
			}
			return true;
		}
		return false;
	}
	
	public Theme getTheme(){
		if(theme == null){
			setTheme(THEME_STYLE_FASHION);
		}
		return theme;
	}
	
	public int getCurrentThemeStyle(){
		return currentThemeStyle;
	}

}
