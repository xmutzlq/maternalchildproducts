package com.beat.android.maternalchild.common;

public class AppBroadcast {
	/** 关闭应用程序的ACTION */
	public static final String ACTION_CLOSE_APP = "com.beat.android.maternalchild.action.CLOSE_APP";
	/** 用户登录状态改变的ACTION */
	public static final String ACTION_USER_LOGIN_STATE_CHANGE = "com.beat.android.maternalchild.action.ACTION_USER_LOGIN_STATE_CHANGE";
	/** 用户信息状态改变的ACTION */
	public static final String ACTION_USER_INFO_STATE_CHANGE = "com.beat.android.maternalchild.action.ACTION_USER_INFO_STATE_CHANGE";
	
	/** 通知更换皮肤ACTION */
	public static final String ACTION_THEME_CHANGE = "com.beat.android.maternalchild.action.THEME_CHANGE";
	/** 通知简繁体变化 */
	public static final String ACTION_LANGUAGE_CHANGE = "com.beat.android.maternalchild.action.LANGUAGE_CHANGE";
}
