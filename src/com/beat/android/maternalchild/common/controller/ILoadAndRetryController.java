package com.beat.android.maternalchild.common.controller;

import com.beat.android.maternalchild.os.INetAsyncTask;

public interface ILoadAndRetryController {
	public static final int LOAD_VIEW_TYPE_GONE = -1;
	public static final int LOAD_VIEW_TYPE_LOADING = 0;
	public static final int LOAD_VIEW_TYPE_RETRY = 1;
	public static final int LOAD_VIEW_TYPE_SETTING_NET = 2;
	/**
	 * 尝试启动任务，此方法反正无网络提示，设置网络后自动重新请求流程
	 * @param lastTask 只保留最后一次任务
	 * @return 当前调用是否启动成功
	 */
	public boolean tryStartNetTack(INetAsyncTask lastTask);
	/**
	 * 设置网络请求任务
	 * @param lastTask 只保留最后一次任务
	 */
	public void setNetTack(INetAsyncTask lastTask);
	/**
	 * 派遣网络监听变更事件
	 * @param isAvailable
	 */
	public void dispatchNetworkChange(boolean isAvailable);
	
	public boolean checkNetWrok();
	
	public void showNetSettingView();
	
	public void showRetryView();
	
	public void showLoadingView();

	public void hideLoadAndRetryView();
	
	public boolean isTransparentLoadingView();
}
