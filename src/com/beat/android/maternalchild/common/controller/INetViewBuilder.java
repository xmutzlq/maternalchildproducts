package com.beat.android.maternalchild.common.controller;

import android.view.View.OnClickListener;

public interface INetViewBuilder {
	public void onShowLoadingView();
	public void onHideLoadAndRetryView(int lastLoadViewType);
	public void onShowRetryView(OnClickListener retryListener);
	public void onShowNetSettingView(OnClickListener netSettingListener, OnClickListener gotoBookShelfListener);
}
