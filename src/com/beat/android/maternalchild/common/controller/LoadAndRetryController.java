package com.beat.android.maternalchild.common.controller;

import com.beat.android.maternalchild.os.INetAsyncTask;
import com.beat.android.maternalchild.utils.NetWorkUtil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

public class LoadAndRetryController implements ILoadAndRetryController {

	public static int mLastWisdomArrIndex = -1;
	private int mCurrentLoadViewType = LOAD_VIEW_TYPE_GONE;
	private INetAsyncTask mLastTask;
	private INetViewBuilder mBuilder;
	private Context mContext;
	private OnClickListener mOnClickListener;
	
	public LoadAndRetryController(Context context, INetViewBuilder builder){
		mBuilder = builder;
		mContext = context;
	}
	/**
	 * 获得重试按钮点击事件
	 * @return
	 */
	private OnClickListener getReLoadButClickListener(){
		if(mOnClickListener == null){
			mOnClickListener = new OnClickListener() {
				@Override
				public void onClick(View v) {
					switch (mCurrentLoadViewType) {
					case LOAD_VIEW_TYPE_RETRY:
						dispatchRetry();
						break;
					case LOAD_VIEW_TYPE_SETTING_NET:
						dispatchSettingNet();
						break;
					}
				}
			};
		}
		return mOnClickListener;
	}
	
	/**
	 * 派遣设置网络事件
	 */
	private void dispatchSettingNet(){
		//打开网络帮助界面
		Intent intent = null;
        //判断手机系统的版本  即API大于10 就是3.0或以上版本 
        if(android.os.Build.VERSION.SDK_INT>10){
            intent = new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS);
        }else{
            intent = new Intent();
            ComponentName component = new ComponentName("com.android.settings","com.android.settings.WirelessSettings");
            intent.setComponent(component);
            intent.setAction("android.intent.action.VIEW");
        }
        mContext.startActivity(intent);
	}
	/**
	 * 派遣重试事件
	 */
	private void dispatchRetry(){
		if(!onRetry()){
			if(mLastTask != null){
				tryStartNetTack(mLastTask);
			}
		}
	}
	/**
	 * 点击重试按钮回调
	 * @return 返回true代表子类已经处理，父类不再执行重试流程
	 */
	protected boolean onRetry(){
		return false;
	}
	/**
	 * 检测网络，如果没有网络提示设置网络
	 * @return true 表示有网络
	 */
	@Override
	public boolean checkNetWrok() {
		if(!NetWorkUtil.isNetAvailable(mContext)){
			showNetSettingView();
			return false;
		}
		return true;
	}
	
	@Override
	public boolean tryStartNetTack(INetAsyncTask lastTask) {
		if(lastTask == null){
			return false;
		}
		mLastTask = lastTask;
		if(NetWorkUtil.isNetAvailable(mContext)){
			if(mLastTask.isStop()){
				mLastTask.start();
				return true;
			}
		}else{
			showNetSettingView();
		}
		return false;
	}

	@Override
	public void setNetTack(INetAsyncTask lastTask) {
		mLastTask = lastTask;
	}

	@Override
	public void dispatchNetworkChange(boolean isAvailable) {
		if(isAvailable){
			if(mCurrentLoadViewType == LOAD_VIEW_TYPE_SETTING_NET){
				hideLoadAndRetryView();
			}
			if(mLastTask != null && mLastTask.isNeedReStart()){
				tryStartNetTack(mLastTask);
			}
		}
	}

	/**
	 * 显示网络设置提示界面
	 */
	@Override
	public void showNetSettingView() {
		if(mCurrentLoadViewType == LOAD_VIEW_TYPE_SETTING_NET){
			return;
		}
		mCurrentLoadViewType = LOAD_VIEW_TYPE_SETTING_NET;
		mBuilder.onShowNetSettingView(getReLoadButClickListener(),new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
			}
		});
	}
	
	/**
	 * 显示重试界面
	 */
	@Override
	public void showRetryView() {
		if(mCurrentLoadViewType == LOAD_VIEW_TYPE_RETRY){
			return;
		}
		mCurrentLoadViewType = LOAD_VIEW_TYPE_RETRY;
		mBuilder.onShowRetryView(getReLoadButClickListener());
	}

	/**
	 * 显示加载界面
	 */
	@Override
	public void showLoadingView() {
		if(mCurrentLoadViewType == LOAD_VIEW_TYPE_LOADING){
			return;
		}
		mCurrentLoadViewType = LOAD_VIEW_TYPE_LOADING;
		mBuilder.onShowLoadingView();
	}

	/**
	 * 隐藏（加载/重试/设置网络）界面
	 */
	@Override
	public void hideLoadAndRetryView() {
		if(mCurrentLoadViewType == LOAD_VIEW_TYPE_GONE){
			return;
		}
		int oldType = mCurrentLoadViewType;
		mCurrentLoadViewType = LOAD_VIEW_TYPE_GONE;
		mBuilder.onHideLoadAndRetryView(oldType);
	}

	@Override
	public boolean isTransparentLoadingView() {
		return mLastTask != null && !mLastTask.isNeedReStart();
	}
}
